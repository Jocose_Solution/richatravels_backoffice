﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateTicket_PNR.aspx.cs" Inherits="GroupSearch_UpdateTicket_PNR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
          <script language="javascript" type="text/javascript">
              function focusObj(obj) {
                  if (obj.value == "Airline PNR") obj.value = "";
              }
              function blurObj(obj) {
                  if (obj.value == "") obj.value = "Airline PNR";
              }
              function focusObj1(obj) {
                  if (obj.value == "GDS PNR") obj.value = "";
              }
              function blurObj1(obj) {
                  if (obj.value == "") obj.value = "GDS PNR";
              }
              function focusObj2(obj) {
                  if (obj.value == "Ticket No.") obj.value = "";
              }
              function blurObj2(obj) {
                  if (obj.value == "") obj.value = "Ticket No.";
              }
    </script>
    <script language="javascript" type="text/javascript">
        function Validate() {
            if ((document.getElementById("txt_gdspnr").value == "" || document.getElementById("txt_INGDS").value == "GDS PNR")
                || (document.getElementById("txt_INGDS").value == "")) {
                alert('GDS PNR can not be blank,Please fill the gds pnr name');
                return false;
            }
            if ((document.getElementById("txt_airlinepnr").value == "") || (document.getElementById("txt_INAirLine").value == "Airline PNR")) {
                alert('Airline PNR can not be blank or 0,Please fill airline pnr');
                return false;
            }
            $("#waitMessage").show();
        }
    </script>
        <style type="text/css">
            .page-wrapperss {
                background-color: #fff;
                margin-left: 15px;
            }

            .overfl {
                overflow-y: scroll;
            }
        </style>
         
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                <div class="page-wrapperss">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body">
                               <div id="Div_Exec" runat="server">
            <div></div>
            <div id="div_flightdetails" runat="server">
                <table border="1">
                    <tr>
                        <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Flight Details</td>
                    </tr>
                    <tr>
                        <td style='font-size: 13px; width: 15%; text-align: left;' colspan="8">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="FlightDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" Width="100%"
                                        PageSize="100">
                                        <Columns>
                                            <asp:TemplateField HeaderText="RequestID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Departure">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeparture_Location" runat="server" Text='<%#Eval("Departure_Location")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Dep Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeparture_Date" runat="server" Text='<%#Eval("Departure_Date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Dep Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeparture_Time" runat="server" Text='<%#Eval("Departure_Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Arrival">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrival_Location" runat="server" Text='<%#Eval("Arrival_Location")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Arvl Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrival_Date" runat="server" Text='<%#Eval("Arrival_Date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Arvl Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrival_Time" runat="server" Text='<%#Eval("Arrival_Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Aircode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAircode" runat="server" Text='<%#Eval("Aircode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Flight No">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFlightNumber" runat="server" Text='<%#Eval("FlightNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div></div>
            <div id="div1" runat="server">
                <table border="1">
                    <tr>
                        <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Booking Details</td>
                    </tr>
                    <tr>
                        <td style='font-size: 13px; width: 15%; text-align: center;'>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="BookingDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" Width="100%"
                                        PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Trip Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Adult">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAdultCount" runat="server" CssClass="" Text='<%#Eval("AdultCount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Child">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblChildCount" runat="server" Text='<%#Eval("ChildCount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Infant">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInfantCount" runat="server" Text='<%#Eval("InfantCount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ExpectedPrice">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExpactedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Partner Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpartnerparice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Remarks")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_status" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" BackColor="#014c90" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
            <div></div>
        </div>
        <div id="div_BookingDetails" runat="server">
            <table border="1" width="100%">
                <tr>
                    <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Booking Details</td>
                </tr>
                <tr>
                    <td style='font-size: 13px; width: 15%; text-align: center;'>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="BookingDetails111" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" Width="100%"
                                    PageSize="30">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Trip Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adult">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAdultCount" runat="server" CssClass="" Text='<%#Eval("AdultCount")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Child">
                                            <ItemTemplate>
                                                <asp:Label ID="lblChildCount" runat="server" Text='<%#Eval("ChildCount")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Infant">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInfantCount" runat="server" Text='<%#Eval("InfantCount")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ExpactedPrice">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExpactedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Partner Price">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpartnerparice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Remarks")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_status" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" BackColor="#014c90" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divpnr" runat="server">
            <table width="50%">
                <tr>
                    <td>Pnr Details:
                    </td>
                </tr>
                <tr>
                    <td>Airline PNR :<asp:TextBox ID="txt_airlinepnr" onfocus="focusObj(this);" onblur="blurObj(this);" Text="Airline PNR" MaxLength="8" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                        &nbsp;
                    </td>
                    <td>GDS PNR : 
                        <asp:TextBox ID="txt_gdspnr" MaxLength="8" onfocus="focusObj1(this);" onblur="blurObj1(this);" Text="GDS PNR" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                        &nbsp;</td>
                </tr>
            </table>
            <table width="50%" id="tbl_inboundpnr" runat="server" visible="false">
                <tr>
                    <td>InBound Pnr Details:
                    </td>
                </tr>
                <tr>
                    <td>Airline PNR :<asp:TextBox ID="txt_INAirLine" MaxLength="8" onfocus="focusObj(this);" onblur="blurObj(this);" Text="Airline PNR" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                        &nbsp;
                    </td>
                    <td>GDS PNR : 
                        <asp:TextBox ID="txt_INGDS" MaxLength="8" onfocus="focusObj1(this);" onblur="blurObj1(this);" Text="GDS PNR" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                        &nbsp;</td>
                </tr>
            </table>
        </div>
        <div>
            <table width="50%">
                <tr>
                    <td>Pax info with Ticket No:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="UpdateTicket" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" Width="100%" PageSize="100">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Title" FooterStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FirstName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_LastName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket Number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt_Ticket" MaxLength="10" onkeypress="return checkit(event)" onfocus="focusObj2(this);" onblur="blurObj2(this);" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table width="50%" id="tbl_Inboundpaxinfo" runat="server" visible="false">
                <tr>
                    <td>InBound Pax info with Ticket No:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="InbounTicketUpdate" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" Width="100%" PageSize="100">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Title" FooterStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_FirstName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_LastName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket Number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txt_Ticket" MaxLength="10" onkeypress="return checkit(event)" onfocus="focusObj2(this);" onblur="blurObj2(this);" CssClass="ticket" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table width="50%">
                <tr>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server" CssClass="loader" Text="Update" Width="150px" OnClick="btn_update_Click" OnClientClick="return Validate();" />
                    </td>
                </tr>
            </table>
        </div>

                        <%--    <div id="Div_Exec" runat="server">
                                <div></div>
                                <div id="div_flightdetails" runat="server">
                                    <table border="1">
                                        <tr>
                                            <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Flight Details</td>
                                        </tr>
                                        <tr>
                                            <td style='font-size: 13px; width: 15%; text-align: left;' colspan="8">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="FlightDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                                            AutoGenerateColumns="False" CssClass="table " GridLines="None" Width="100%"
                                                            PageSize="30">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="RequestID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestID")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Departure">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDeparture_Location" runat="server" Text='<%#Eval("Departure_Location")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Dep Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDeparture_Date" runat="server" Text='<%#Eval("Departure_Date")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Dep Time">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDeparture_Time" runat="server" Text='<%#Eval("Departure_Time")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Arrival">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblArrival_Location" runat="server" Text='<%#Eval("Arrival_Location")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Arvl Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblArrival_Date" runat="server" Text='<%#Eval("Arrival_Date")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Arvl Time">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblArrival_Time" runat="server" Text='<%#Eval("Arrival_Time")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aircode">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAircode" runat="server" Text='<%#Eval("Aircode")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Flight No">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFlightNumber" runat="server" Text='<%#Eval("FlightNumber")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Trip">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />

                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div></div>
                                <div id="div1" runat="server">
                                    <table border="1">
                                        <tr>
                                            <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Booking Details</td>
                                        </tr>
                                        <tr>
                                            <td style='font-size: 13px; width: 15%; text-align: center;'>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                     
                                                        <asp:GridView ID="BookingDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                                            AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                                            PageSize="30">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Trip Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Adult">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAdultCount" runat="server" CssClass="" Text='<%#Eval("AdultCount")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Child">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblChildCount" runat="server" Text='<%#Eval("ChildCount")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Infant">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblInfantCount" runat="server" Text='<%#Eval("InfantCount")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ExpectedPrice">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblExpactedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Partner Price">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblpartnerparice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Remarks">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Remarks")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_status" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                            <AlternatingRowStyle CssClass="AltRowStyle" BackColor="#014c90" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div></div>

                            </div>
                            <div id="div_BookingDetails" runat="server">
                                <table border="1" width="100%">
                                    <tr>
                                        <td style='font-size: 13px; width: 15%; text-align: left; color: #FFFFFF; padding: 5px; font-weight: bold; background-color: #014c90;'>Booking Details</td>
                                    </tr>
                                    <tr>
                                        <td style='font-size: 13px; width: 15%; text-align: center;'>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="BookingDetails111" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                                        PageSize="30">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Trip Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Adult">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdultCount" runat="server" CssClass="" Text='<%#Eval("AdultCount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Child">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblChildCount" runat="server" Text='<%#Eval("ChildCount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Infant">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblInfantCount" runat="server" Text='<%#Eval("InfantCount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ExpactedPrice">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExpactedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Partner Price">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpartnerparice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Remarks">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Remarks")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_status" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" BackColor="#014c90" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divpnr" runat="server">
                                <table width="50%">
                                    <tr>
                                        <td>Pnr Details:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Airline PNR :<asp:TextBox ID="txt_airlinepnr" MaxLength="8" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td>GDS PNR : 
                        <asp:TextBox ID="txt_gdspnr" MaxLength="8" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                <table width="50%" id="tbl_inboundpnr" runat="server" visible="false">
                                    <tr>
                                        <td>InBound Pnr Details:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Airline PNR :<asp:TextBox ID="txt_INAirLine" MaxLength="8" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                                            &nbsp;
                                        </td>
                                        <td>GDS PNR : 
                        <asp:TextBox ID="txt_INGDS" MaxLength="8" onkeypress="return checkit(event)" runat="server"></asp:TextBox>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table width="50%">
                                    <tr>
                                        <td>Pax info with Ticket No:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="UpdateTicket" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%" PageSize="30">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Title" FooterStyle-Wrap="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                                    <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter")%>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="First Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FirstName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Last Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_LastName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Pax Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ticket Number">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txt_Ticket" MaxLength="10" onkeypress="return checkit(event)" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table width="50%" id="tbl_Inboundpaxinfo" runat="server" visible="false">
                                    <tr>
                                        <td>InBound Pax info with Ticket No:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="InbounTicketUpdate" runat="server" AllowPaging="True" AllowSorting="True"
                                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%" PageSize="30">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Title" FooterStyle-Wrap="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_title" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                                    <asp:Label ID="lbl_counter" runat="server" Text='<%#Eval("Counter")%>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="First Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_FirstName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Last Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_LastName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Pax Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Ticket Number">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txt_Ticket" MaxLength="10" onkeypress="return checkit(event)" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>--%>
                          <%--  <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <table width="50%">
                                            <tr>
                                                <td align="right">
                                                    <asp:Button ID="btn_update" runat="server" CssClass="button buttonBlue" Text="Update" OnClick="btn_update_Click" OnClientClick="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>

                </div>

            </div>
        </div>




        <div id="waitMessage" style="display: none;">
       
        <div class="" style="text-align: center; opacity: 0.7; position: absolute; z-index: 99999; top: 10px; width: 1020px; height: 100%; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border: 5px solid #d1d1d1; border-radius: 10px;">
            Please wait....<br />
            <br />
            <img alt="loading" src="<%=ResolveUrl("~/images/loadingAnim.gif")%>" />
            <br />
        </div>
    </div>
   <%-- <script type="text/javascript">
        $(".loader").click(function (e) {
            $("#waitMessage").show();
        });

    </script>
    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58 || charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;
        }
        //function CloseAndRefresh() {
        //    alert("jkjlkjkjjkjjhgkjjh");
        //    open(location, '_self').close();
        //    window.open("GroupSearch/ExecRequestDetails.aspx");
        //}
        function MyFunc(strmsg) {
            switch (strmsg) {
                case 1: {
                    alert("Tickted updated!!");
                    window.opener.location.reload('ExecRequestDetails.aspx');
                    window.close();
                }
                    break;
                case 2: {
                    alert("Ticket Updated Successfully, but E-mailID not found !!");
                    window.opener.location.reload('ExecRequestDetails.aspx');
                    window.close();
                }
                    break;
            }
        }
    </script>--%>
          <script type="text/javascript">
              function checkit(evt) {
                  evt = (evt) ? evt : window.event
                  var charCode = (evt.which) ? evt.which : evt.keyCode
                  if (!(charCode > 47 && charCode < 58 || charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                      return false;
                  }
                  status = "";
                  return true;
              }
              function MyFunc(strmsg) {
                  switch (strmsg) {
                      case 1: {
                          alert("Tickted updated!!");
                          window.opener.location.reload('ExecRequestDetails.aspx');
                          window.close();
                      }
                          break;
                      case 2: {
                          alert("Ticket Updated Successfully, but E-mailID not found !!");
                          window.opener.location.reload('ExecRequestDetails.aspx');
                          window.close();
                      }
                          break;
                  }
              }
    </script>
    </form>
</body>
</html>
