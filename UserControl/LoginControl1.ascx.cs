﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;

public partial class UserControl_LoginControl1 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {   try
        {
            LblIPAddress.Text = GetIPAddress();
            REMOTE_ADDR.Text = GetIPAddressREMOTE_ADDR();
            HTTP_X_FORWARDED_FOR.Text = GetIPAddressFORWARDED_FOR();
            UserLocalHost.Text = GetIPAddressUserHostAddress();
        }
        catch(Exception ex)
        {
            LblIPAddress.Text = "ExMessage:" + ex.Message + " ,StackTrace:" + ex.StackTrace;
        }
             
        
    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        
        if (string.IsNullOrEmpty(UserName.Text))
        {
            UserName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid Userid.');", true);
            return;
        }

        if (string.IsNullOrEmpty(Password.Text))
        {
            Password.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid Password.');", true);
            return;
        }


        Pro p1 = new Pro();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        
        SqlDataAdapter adp = new SqlDataAdapter();
        string IPAddress = GetIPAddress();       
        if (!string.IsNullOrEmpty(Password.Text) && !string.IsNullOrEmpty(UserName.Text))
        {
            string IpValidate = CheckIPAddressForLogin(UserName.Text, IPAddress);
            if (IpValidate == "true")
            {
                try
                {
                    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
                    SqlConnection con = new SqlConnection(constr);
                    SqlCommand cmd = new SqlCommand("UserLogin_PP");
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Usename", UserName.Text);
                    cmd.Parameters.AddWithValue("@Password", Password.Text);
                    adp.SelectCommand = cmd;
                    int temp = adp.Fill(ds);
                    cmd.Dispose();
                    //if (ds.Tables[0].Rows.Count > 0)
                    if (temp == 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp", "<script type='text/javascript'>alert('Please enter valid Password.');</script>", false);
                        UserName.Text = "";
                    }
                    else
                    {
                        if (ds.Tables[0].Rows[0]["Branch"].ToString().ToUpper() == "MUMBAI")
                        {
                            Response.Redirect("http://admin.flywidus.co/mumbai/AccessPermissionPP.aspx?UserID=" + UserName.Text + "&Code=" + Password.Text);
                        }
                        else
                        {

                            Session["_PASSWORD"] = Password.Text;
                            Session["_USERNAME"] = UserName.Text;
                            Session["TripExec"] = "D";
                            Session["UID"] = UserName.Text;
                            Session["Password"] = Password.Text;
                            Session["User_Type"] = ds.Tables[0].Rows[0]["role_type"].ToString();
                            Session["Role"] = ds.Tables[0].Rows[0]["Role"].ToString();
                            Session["Branch"] = ds.Tables[0].Rows[0]["Branch"].ToString();

                            try
                            {
                                Session["DExport"] = StatusExport();
                                Session["EditPermit"] = StatusEdit();
                                Session["PrintPermit"] = StatusPrint();
                            }
                            catch (Exception ex)
                            {
                                Session["DExport"] = false;
                                Session["EditPermit"] = false;
                            }
                            Session["Role_Id"] = ds.Tables[0].Rows[0]["role_id"].ToString();
                            Session["username"] = ds.Tables[0].Rows[0]["name"].ToString();
                            if (Convert.ToString(Session["User_Type"]) == "ACC")
                            {
                                Session["TypeID"] = "AC1";
                                Session["UserType"] = "AC";
                            }
                            else if (Convert.ToString(Session["User_Type"]) == "EXEC")
                            {
                                Session["TypeID"] = "EC1";
                                Session["UserType"] = "EC";
                            }
                            else if (Session["User_Type"].ToString() == "ADMIN")
                            {
                                Session["TypeID"] = "AD1";
                                Session["UserType"] = "AD";
                            }
                            else if (Session["User_Type"].ToString() == "SALES")
                            {
                                Session["TypeID"] = Session["User_Type"];
                                Session["UserType"] = Session["User_Type"];
                            }


                            Session["ModeTypeITZ"] = "WEB";
                            Session["MchntKeyITZ"] = ConfigurationManager.AppSettings["MerchantKey"].ToString();
                            Session["_SvcTypeITZ"] = "MERCHANTDB";
                            if (Session["User_Type"].ToString() == "SALES")
                            {
                                Response.Redirect("SprReports/Admin/Agent_Details.aspx", false);
                            }
                            else
                            {
                                Response.Redirect("Dashboard.aspx", false);
                            }
                        }
                    }




                    //if (ds.Tables[0].Rows.Count > 0),
                    //{
                    //    Response.Redirect("IBEHome.aspx");
                    //}


                    //dt = ds.Tables["ExecuRegister"];
                    //if (dt.rows.Count > 0)
                    //{

                    //    Response.Redirect("IBEHome.aspx");
                    //}

                }
                catch (Exception ex)
                {
                    //Label lblerr = (Label)UserLogin.FindControl("lblerror");
                    //lblerr.Text = "SQL ERROR";
                }
            }
            else
            {
                //set message
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please try again.');", true);
                return;
            }
        }
        else
        {
            UserName.Text = "";
            Password.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid user id and Password.');", true);
            return;
        }

    }
    public bool StatusExport()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();

        bool ret = false;
        try
        {
            SqlCommand cmd = new SqlCommand("Sp_GetExportPermission", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", Session["UID"]);
            cmd.Parameters.AddWithValue("@Role", Session["Role"]);  
          
            ret = Convert.ToBoolean(cmd.ExecuteScalar());
            return ret;
        }
        catch (Exception info)
        {
            throw info;
           
        }
        finally
        {
            con.Close();
            con.Dispose();
           
        }
    }

    public bool StatusEdit()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();

        bool ret = false;
        try
        {
            SqlCommand cmd = new SqlCommand("Sp_GetExportPermissionforEdit", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", Session["UID"]);
            cmd.Parameters.AddWithValue("@Role", Session["Role"]);

            ret = Convert.ToBoolean(cmd.ExecuteScalar());
            return ret;
        }
        catch (Exception info)
        {
            throw info;

        }
        finally
        {
            con.Close();
            con.Dispose();

        }
    }
    public bool StatusPrint()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();

        bool ret = false;
        try
        {
            SqlCommand cmd = new SqlCommand("Sp_GetExportPermissionforPrint", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", Session["UID"]);
            cmd.Parameters.AddWithValue("@Role", Session["Role"]);

            ret = Convert.ToBoolean(cmd.ExecuteScalar());
            return ret;
        }
        catch (Exception info)
        {
            throw info;

        }
        finally
        {
            con.Close();
            con.Dispose();

        }
    }

    protected string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {        
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];        
        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                    IpAddress= addresses[0];
            }
         }
        else
            {
                IpAddress= context.Request.ServerVariables["REMOTE_ADDR"];
            }     
       }
        catch(Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressREMOTE_ADDR()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {
            IpAddress = context.Request.ServerVariables["REMOTE_ADDR"];
        }
        catch (Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressFORWARDED_FOR()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {
            IpAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        }
        catch (Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressUserHostAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string visitorIPAddress = string.Empty;
        //try
        //{
        //    IpAddress = context.Request.UserHostAddress;
        //}
        //catch (Exception ex)
        //{

        //}      

        //if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
        //{
            //This is for Local(LAN) Connected ID Address
            string stringHostName = Dns.GetHostName();
            //Get Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;

            try
            {
                visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
            }
            catch
            {
                try
                {
                    visitorIPAddress = arrIpAddress[0].ToString();
                }
                catch
                {
                    try
                    {
                        arrIpAddress = Dns.GetHostAddresses(stringHostName);
                        visitorIPAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        visitorIPAddress = "127.0.0.1";
                    }
                }
            }
       // }
            return visitorIPAddress;

    }

    protected string CheckIPAddressForLogin( string UserId,string ipAddress)
    {
        string login = "false";
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        try
        {
            SqlDataAdapter adp = new SqlDataAdapter();
            DataSet IPDT = new DataSet();
           
            SqlCommand cmd = new SqlCommand("CheckLoginIpAddress");
            cmd.Connection = con;
            if (con.State != ConnectionState.Open)
                con.Open();           
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
            adp.SelectCommand = cmd;
            int temp = adp.Fill(IPDT);
            cmd.Dispose();
            if (con.State != ConnectionState.Closed)
                con.Close();

            if (IPDT != null && IPDT.Tables.Count > 0 && IPDT.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(IPDT.Tables[0].Rows[0]["IPCount"])) && Convert.ToInt32(IPDT.Tables[0].Rows[0]["IPCount"]) > 0 && Convert.ToInt32(IPDT.Tables[0].Rows[0]["RCount"]) > 0)
                {
                    login = "true";
                }
                else
                {
                    login = "false";
                }
            }
            else
            {
                login = "false";
            }
        }
        catch (Exception ex)
        {            
            if (con.State != ConnectionState.Closed)
                con.Close();
            login = "false";
        }
        return login;
    }

}