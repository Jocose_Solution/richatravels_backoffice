﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_RefundFailPNR : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    SqlTransactionDom STDom = new SqlTransactionDom();
    DataSet dst = new DataSet();
    string transid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            dst = BindGridview(transid);
        }
    }



    public static string excelCS;

    protected void btnledger_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_refundFailDirectPNR", con);
            cmd.CommandType = CommandType.StoredProcedure;
            
            //cmd.CommandTimeout = 120;
            // cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            //int id = (int)cmd.Parameters["@i"].Value;          
            con.Close();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ledger Inserted Successfully')", true);

        }

        catch (Exception ex)
        {

            Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
        }
        BindGridview(transid);

    }



    public DataSet BindGridview(string transid)
    {

        using (SqlConnection con = new SqlConnection(CS))
        {
            transid = texttransid.Value.ToString();
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "FailedPNR");
            cmd.Parameters.AddWithValue("@transid", transid);
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sdr.Fill(ds);
            con.Open();

            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
            lbl_counttkt.Text = ds.Tables[0].Rows.Count.ToString();

            return ds;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        transid = texttransid.Value.ToString();
        dst = BindGridview(transid);

            GridView1.DataSource = dst.Tables[0];
            GridView1.DataBind();
            lbl_counttkt.Text = dst.Tables[0].Rows.Count.ToString();


    }

    protected void btnrefund_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(texttransid.Value.ToString()))
            {
                SqlConnection con = new SqlConnection(CS);
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_refundFailDirectPNR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@transid", texttransid.Value.ToString());
                //cmd.CommandTimeout = 120;
                // cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                //int id = (int)cmd.Parameters["@i"].Value;
                con.Close();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ledger Inserted Successfully')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ReservationID cannot be empty')", true);
            }
        }

        catch (Exception ex)
        {

            Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
        }
        BindGridview(transid);
    }


    protected void btn_export_Click(object sender, EventArgs e)
    {
        transid = texttransid.Value.ToString();
        dst = BindGridview(transid);
        STDom.ExportData(dst);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

            GridView1.PageIndex = e.NewPageIndex;
            BindGridview(transid);

    }
}