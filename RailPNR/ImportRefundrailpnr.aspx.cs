﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_ImportRefundrailpnr : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridview();
        }
        
    }
    public static string excelCS;
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.PostedFile != null)
        {
            try
            {

                string path = string.Concat(Server.MapPath("~/RefundUploadfile/" + FileUpload1.FileName));
                FileUpload1.SaveAs(path);
                //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";

                //string excelCS = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                excelCS = @"provider=microsoft.jet.oledb.4.0;data source=" + path + ";extended properties=" + "\"excel 5.0;hdr=yes;\"";


                using (OleDbConnection con = new OleDbConnection(excelCS))
                {
                    OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                    con.Open();
                    // Create DbDataReader to Data Worksheet  
                    DbDataReader dr = cmd.ExecuteReader();
                    // SQL Server Connection String  

                    // Bulk Copy to SQL Server   
                    SqlBulkCopy bulkInsert = new SqlBulkCopy(CS);
                    bulkInsert.DestinationTableName = "Tbl_dailyRailcanceldata";
                    bulkInsert.WriteToServer(dr);
                    BindGridview();
                    lblMessage.Text = "Your file uploaded successfully";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Your file not uploaded";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblmsg.Text = ex.Message;
            }
        }
    }


    private void BindGridview()
    {
        string message = "";
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "REFUND");
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sdr.Fill(ds);
            con.Open();
   
            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
            lblmsg.Text = string.Empty;
            if (ds.Tables[0].Rows.Count != 0)
            {
             
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                     message += ds.Tables[1].Rows[i]["TRANSACTION_ID"].ToString() + '(' + ds.Tables[1].Rows[i]["TRANSACTION_IDCOUNT"].ToString() + ')' + ',';
                }
            }
            if (!string.IsNullOrEmpty(message))
            {
                lblmsg.Text = message + "Please Check these TransactionId!Then Insert Into Ledger ";
            }

        }
    }


    protected void btnledger_Click(object sender, EventArgs e)
    {

        try
        {
            //string message = "";
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_refundrailpnr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter returnParameter = cmd.Parameters.Add("@return", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            int id = (int) returnParameter.Value;
            

            con.Close();
            if (id > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ledger Inserted Successfully')", true);
            }
            else {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Already Exists')", true);
            }
            
        }
        catch (Exception ex)
        {

            Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
        }
        BindGridview();

    }
    protected void btnremove_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "CLEARREFUND");
            con.Open();
            cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Cleared Please ReImport')", true);
            con.Close();
            BindGridview();
        }
    }
}