﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="BusAdminMarkup.aspx.cs" Inherits="BS_BusAdminMarkup" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<%--    <script type="text/javascript" src="../../js/chrome.js"></script>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>

      <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />



    <script type="text/javascript">
        function phone_vali() {
            if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
    </script>



    <div class="row">
           <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Admin Markup</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Type</label>
                                    <asp:DropDownList ID="DropDownListType" runat="server" AppendDataBoundItems="true" CssClass="form-control">
                                        <asp:ListItem Text="ALL" Value="ALL" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">MarkUp value</label>
                                     <asp:TextBox runat="server" ID="mkv" MaxLength="6" onkeypress="phone_vali();" CssClass="form-control"></asp:TextBox> 
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Provide Markup value" ControlToValidate="mkv" ValidationGroup="reqval"></asp:RequiredFieldValidator>                                  
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">MarkUpType</label>
                                    <asp:DropDownList ID="ddl_mktyp" runat="server"  CssClass="form-control">
                                        <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                                        <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                                    </asp:DropDownList>                           
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList ID="ddl_status1" runat="server"  CssClass="form-control">
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">Inactive</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="btnAdd_Click"   CssClass="button buttonBlue" Text="Add New"  ValidationGroup="reqval"/>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Label ID="lbl" runat="server" Style="color: #CC0000;"></asp:Label>
                                </div>
                            </div>
                        </div>


                         <div class="row" id="divReport" style="background-color: #fff; overflow-y: auto; overflow-x: auto; max-height: 500px;" runat="server">
                            <div class="col-md-12">
                        <table style="width: 100%;" cellspacing="10">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP" runat="server">
                                        <ContentTemplate>
                                            <div class="clear1"></div>
                                            <div class="large-12 medium-12 small-12">
                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                                    OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                                CssClass="table " Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ShowEditButton="True" />
                                                        <asp:TemplateField HeaderText="Sr.No">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agent Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_AGENT_TYPE" runat="server" Text='<%# Eval("AGENT_TYPE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Mark Up">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_MARKUP_VALUE" runat="server" Text='<%# Eval("MARKUP_VALUE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txt_MARKUP_VALUE" runat="server" Text='<%# Eval("MARKUP_VALUE")%>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="MarkUpType">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_MarkUpType" runat="server" Text='<%# Eval("MARKUP_Type1")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="DropDownList1" runat="server">
                                                                    <asp:ListItem Value="F">Fixed (F)</asp:ListItem>
                                                                    <asp:ListItem Value="P">Percentage (P)</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Created Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_CREATED_DATE" runat="server" Text='<%# Eval("CREATED_DATE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("MARKUP_ON")) %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlstatus" runat="server" Width="200px" SelectedValue='<%# Eval("MARKUP_ON").ToString() %>'>
                                                                    <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                                                    <asp:ListItem Value="0" Text="Inactive"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:CommandField ShowDeleteButton="True" />
                                                    </Columns>
                                                    <RowStyle CssClass="RowStyle" />
                                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                    <PagerStyle CssClass="PagerStyle" />
                                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                    <HeaderStyle CssClass="HeaderStyle" />
                                                    <EditRowStyle CssClass="EditRowStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                        <ProgressTemplate>
                                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                            </div>
                                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                                Please Wait....<br />
                                                <br />
                                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                                <br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                        </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</asp:Content>

