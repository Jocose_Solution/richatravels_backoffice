﻿<%@ Page Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="HtlRefundInProcces.aspx.vb" Inherits="HtlRefundInProcces"  %>
<%@ Register Src="~/UserControl/HotelMenu.ascx" TagPrefix="uc1" TagName="HotelMenu"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
      
     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
      <script src="../../Hotel/JS/HotelRefund.js" type="text/javascript"></script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Accepted hotel refund details</h3>
                    </div>
                    <div class="panel-body">
                       <%--    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                         <div id="htlRfndPopup">
                <div class="refundbox" >
                    <div class="rfndClose">
                        <a href="javascript:closepopup();">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>
                    <div>
                        <span style="float: left; font-size:12px;">
                            RERECT REMARK OF ORDER ID: <input id="OrderIDS" name="OrderIDS" type="text" style="border:0;font-size:12px;" />
                        </span>
                        <br />
                        <textarea id="txtRemark" name="txtRemark" cols="40" rows="4"></textarea>
                    </div>
                    <div align="right">
                    <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="button" 
                            OnClientClick="return RemarkValidate();" style="height: 26px" />
                    </div>
                </div>
            </div></div></div></div>--%>

                        <div class="large-8 medium-8 small-12 columns end">
                        <div class="large-12 medium-12 small-12 heading">
                      
   
            <div id="htlRfndPopup" style="height:100%; ">
                <div class="refundbox" style="width:38%;">
                    <div class="rfndClose">
                        <a href="javascript:closepopup();">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>
                    <div>
                        <span style="float: left; font-size:12px;">
                            RERECT REMARK OF ORDER ID: <input id="OrderIDS" name="OrderIDS" type="text" style="border:0;font-size:12px;" />
                        </span>
                        <br />
                        <textarea id="txtRemark" name="txtRemark" cols="49" rows="2"></textarea>
                    </div>
                    <div align="right" class="large-2 medium-2 small-2">
                    <asp:Button ID="btnRemark" runat="server" Text="Submit" 
                            OnClientClick="return RemarkValidate();"  />
                    </div>
                </div>
            </div></div></div>   
                           
                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" >
                            <div class="col-md-12">
            <asp:GridView ID="InproccesGrd" runat="server" AutoGenerateColumns="False"  CssClass="table" Width="100%">
                <Columns>
                <asp:TemplateField HeaderText="Order ID">
                    <ItemTemplate>
                       <a href='../../Hotel/BookingSummaryHtl.aspx?OrderId=<%#Eval("OrderId")%> &BID=<%#Eval("BookingID")%>'
                         rel="lyteframe" rev="width: 830px; height: 400px; overflow:hidden;" target="_blank"
                         style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #004b91">
                         <asp:Label ID="lblBID" runat="server" Text= '<%#Eval("OrderId")%>'></asp:Label></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Booking ID">
                    <ItemTemplate>
                         <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("BookingID") %>' ForeColor="Red" CommandName="lnkupdate"
                                CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>       
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="HotelName" HeaderText="Hotel Name" />
                <asp:BoundField DataField="RoomName" HeaderText="Room Name" />
                <asp:BoundField  DataField="AgencyName" HeaderText ="Agency Name" />
                <asp:BoundField DataField="NetCost" HeaderText="Net Cost" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                <asp:BoundField DataField="PgTitle" HeaderText="Title" />
                <asp:BoundField DataField="PgFirstName" HeaderText="First Name" />
                <asp:BoundField DataField="PgLastName" HeaderText="Surname" />
                <asp:BoundField DataField="PgEmail" HeaderText="Email_ID" />
                <asp:BoundField DataField="BookingDate" HeaderText="Booking Date" />
                <asp:BoundField DataField="RequestDate" HeaderText="Submit Date" />
                <asp:BoundField HeaderText="Accept Date" DataField="AcceptDate" />
                    <asp:BoundField DataField="Provider" HeaderText="Supplier" />
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <img alt="Reject" title="Reject" src='<%# ResolveClientUrl("~/Images/reject.png") %>'  border="0" onclick="RejectRemark('<%#Eval("OrderId") %>');" />
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
   </div>

                        </div>
                    </div>
                </div>
            </div>
          </div>
     <%--<div class="mtop80"></div>
<div class="large-12 medium-12 small-12">
    <div class="large-3 medium-3 small-12 columns">     
                <uc1:HotelMenu runat="server" ID="Settings"></uc1:HotelMenu>           
    </div>

<div class="large-8 medium-8 small-12 columns end">
            <div class="large-12 medium-12 small-12 heading">
                        <div class="large-12 medium-12 small-12 heading1">
            Accepted hotel refund details
        </div>
   
            <div id="htlRfndPopup" style="height:100%; display:none;">
                <div class="refundbox" style="width:25%;">
                    <div class="rfndClose">
                        <a href="javascript:closepopup();">
                            <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                    </div>
                    <div>
                        <span style="float: left; font-size:12px;">
                            RERECT REMARK OF ORDER ID: <input id="OrderIDS" name="OrderIDS" type="text" style="border:0;font-size:12px;" />
                        </span>
                        <br />
                        <textarea id="txtRemark" name="txtRemark" cols="40" rows="4"></textarea>
                    </div>
                    <div align="right">
                    <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="button" 
                            OnClientClick="return RemarkValidate();" style="height: 26px" />
                    </div>
                </div>
            </div></div></div>
       
        <div class="large-12 medium-12 small-12">
            <asp:GridView ID="InproccesGrd" runat="server" AutoGenerateColumns="False" CssClass="mGrid" Width="100%">
                <Columns>
                <asp:TemplateField HeaderText="Order ID">
                    <ItemTemplate>
                       <a href='../../Hotel/BookingSummaryHtl.aspx?OrderId=<%#Eval("OrderId")%> &BID=<%#Eval("BookingID")%>'
                         rel="lyteframe" rev="width: 830px; height: 400px; overflow:hidden;" target="_blank"
                         style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; color: #004b91">
                         <asp:Label ID="lblBID" runat="server" Text= '<%#Eval("OrderId")%>'></asp:Label></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Booking ID">
                    <ItemTemplate>
                         <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("BookingID") %>' ForeColor="Red" CommandName="lnkupdate"
                                CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>       
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="HotelName" HeaderText="Hotel Name" />
                <asp:BoundField DataField="RoomName" HeaderText="Room Name" />
                <asp:BoundField  DataField="AgencyName" HeaderText ="Agency Name" />
                <asp:BoundField DataField="NetCost" HeaderText="Net Cost" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                <asp:BoundField DataField="PgTitle" HeaderText="Title" />
                <asp:BoundField DataField="PgFirstName" HeaderText="First Name" />
                <asp:BoundField DataField="PgLastName" HeaderText="Surname" />
                <asp:BoundField DataField="PgEmail" HeaderText="Email_ID" />
                <asp:BoundField DataField="BookingDate" HeaderText="Booking Date" />
                <asp:BoundField DataField="RequestDate" HeaderText="Submit Date" />
                <asp:BoundField HeaderText="Accept Date" DataField="AcceptDate" />
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <img alt="Reject" title="Reject" src='<%# ResolveClientUrl("~/Images/reject.png") %>'  border="0" onclick="RejectRemark('<%#Eval("OrderId") %>');" />
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
   
</div>--%>
</asp:Content>