﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Partial Class Reports_Refund_HtlRefundRequest
    Inherits System.Web.UI.Page
    Private ST As New HotelDAL.HotelDA()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", True)
            End If

            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Acept_grdview.DataSource = ST.HtlRefundDetail(StatusClass.Pending.ToString(), "", "", "RefundGET", "")
            Acept_grdview.DataBind()
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
  
    Protected Sub btnRemark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemark.Click
        Try
            'Update Status and Remark in CancellationIntl Table after Reject
            Dim i As Integer = ST.HtlRefundUpdates(StatusClass.Rejected.ToString(), Request("OrderIDS"), Session("UID").ToString, "RejPending", Request("txtRemark").Trim())
            If i > 0 Then
                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Reject succesfully'); ", True)
                BindGrid()
                Try
                    ''''Send Mail and SMS Start
                    Dim HtlDetailsDs As New DataSet()
                    HtlDetailsDs = ST.htlintsummary(Request("OrderIDS"), "Ticket")

                    Dim objmail As New HotelBAL.HotelSendMail_Log()
                    ''objmail.SendEmailForCancelAndReject(Request("OrderIDS"), HtlDetailsDs.Tables(0).Rows(0)("BookingID").ToString(), HtlDetailsDs.Tables(0).Rows(0)("HotelName").ToString(), HtlDetailsDs.Tables(0).Rows(0)("LoginID").ToString(), HotelShared.HotelStatus.HOTEL_REJECT.ToSting(), "Cancellation Reject")

                    Dim objSMSAPI As New SMSAPI.SMS
                    Dim objSqlNew As New SqlTransactionNew
                    Dim smstext As String = ""
                    Dim SmsCrd As DataTable
                    Dim objDA As New SqlTransaction
                    SmsCrd = objDA.SmsCredential(SMS.HOTELBOOKING.ToString()).Tables(0)
                    Dim smsStatus As String = ""
                    Dim smsMsg As String = ""
                    If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                        smsStatus = objSMSAPI.SendHotelSms(Request("OrderIDS"), "", HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), "", "", "", "CancelReject", smstext, SmsCrd)
                        objSqlNew.SmsLogDetails(Request("OrderIDS"), HtlDetailsDs.Tables(1).Rows(0)("GPhoneNo").ToString().Trim(), smstext, smsStatus)
                    End If
                    ''''Send Mail and SMS End
                Catch ex As Exception
                    HotelDAL.HotelDA.InsertHotelErrorLog(ex, " Send SMS Support Refund Request")
                End Try
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub lnkAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs)   
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
            gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
            'Update Status in CancellationIntl Table after Accept
            Dim i As Integer = ST.HtlRefundUpdates(StatusClass.InProcess.ToString(), lb.CommandArgument, Session("UID").ToString, "Modify", "")
            If i > 0 Then
                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "alert('Accept succesfully'); ", True)
                BindGrid()
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
End Class
