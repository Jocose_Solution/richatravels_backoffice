﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="UploadAgentPanel.aspx.vb" Inherits="SprReports_Accounts_UploadAgentPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .buttonBlue {
            float: right;
            margin-top: 0px;
            height: 38px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
        function isChar(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 32) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
        function ValidateAgent() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").value == "") {
                alert('Please Fill Amount');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_amount").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Select Payment Mode") {
                alert('Please Select Payment Mode');
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").focus();
                return false;
            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_city").value == "") {
                    alert('Please Fill Deposite City');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_city").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").value == "") {
                    alert('Please Fill Deposite Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_depositedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").value == "--Select Office--") {
                    alert('Please Select Office');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_Office").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").value == "") {
                    alert('Please Fill Concern Person');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_concernperson").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }


            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cash Deposite In Bank") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").value == "") {
                    alert('Please Fill Branch Code');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BranchCode").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "Cheque") {

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").value == "") {
                    alert('Please Fill Cheque Date');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequedate").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").value == "") {
                    alert('Please Fill Cheque Number');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_chequeno").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").value == "") {
                    alert('Please Fill Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_BankName").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "NetBanking" || document.getElementById("ctl00_ContentPlaceHolder1_ddl_modepayment").value == "RTGS") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").value == "--Select Bank--") {
                    alert('Please Select Bank Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_BankName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").value == "") {
                    alert('Please Fill Transaction ID');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_tranid").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").value == "") {
                    alert('Please Fill Remark');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_remark").focus();
                    return false;
                }

            }

        }
    </script>

    <div class="row">
        <%--<div class="col-md-2"></div>--%>

        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Upload > Agent Upload Request </h3>
                    </div>
                    <div class="panel-body">

                        <div class="">

                            <div class="card-header">
                                <div class="col-md-12">
                                    <h3 style="text-align: center;">Credit Upload Form</h3>
                                    <hr />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 290px" onfocus="focusObj(this);" onblur="blurObj(this);" placeholder="Agency Name or ID" autocomplete="off" class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    <asp:HiddenField ID="hdnAgencyId" runat="server" />
                                    <asp:HiddenField ID="hdnAgencyName" runat="server" />
                                </div>
                                <div class="col-md-2">
                                    <asp:Button ID="btn_search" runat="server" CssClass="button buttonBlue" Text="Search" />
                                </div>
                                <div class="col-md-3">
                                </div>

                            </div>

                            <div class="col-md-12" id="FormFields" runat="server" visible="false">
                                <div class="row">
                                    <table class="w70 auto boxshadow">
                                        <tr style="display: none">

                                            <td align="left" style="color: #fff; font-weight: bold; padding-top: 7px;" colspan="4">Deposit Request Form
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="fltdtls" colspan="4" id="td_UploadType" runat="server" visible="false">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left" height="30" style="font-weight: bold; color: #fff" width="200">Uploade Request Type:
                                                        </td>
                                                        <td>
                                                            <asp:RadioButtonList ID="RBL_UploadType" runat="server" RepeatDirection="Horizontal"
                                                                Width="300px">
                                                                <asp:ListItem Value="CA" Selected="True" style="color: #fff"> Fresh Upload</asp:ListItem>
                                                                <asp:ListItem Value="AD" style="color: #fff"> Adjustment</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="clear"></div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <asp:TextBox ID="txt_amount" runat="server" MaxLength="10" placeholder="Enter Amount" class="form-control" onKeyPress="return isNumberKey(event)"></asp:TextBox>

                                    </div>

                                    <div class="col-md-3 col-xs-6">
                                        <label>Payment Mode</label>
                                        <asp:DropDownList ID="ddl_modepayment" runat="server" placeholder="Select Payment Mode" AutoPostBack="True" class="form-control">
                                        </asp:DropDownList>

                                    </div>


                                    <div id="check_info" runat="server" visible="false">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Cheque Date</label>
                                            <asp:TextBox ID="txt_chequedate" class="form-control" placeholder="Select Cheque Date" runat="server"></asp:TextBox>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Cheque No.</label>
                                            <asp:TextBox ID="txt_chequeno" class="form-control" placeholder="Enter Cheque No." runat="server"></asp:TextBox>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Bank Name</label>
                                            <asp:TextBox ID="txt_BankName" placeholder="Enter Bank Name" runat="server" class="form-control" AutoPostBack="True"></asp:TextBox>

                                        </div>

                                    </div>

                                    <div width="110px" align="left" id="td_Bank" visible="false" runat="server" style="display: none;" class="fltdtls"
                                        height="30px">
                                    </div>

                                    <div class="col-md-3 col-xs-6" id="td_Bank1" visible="false" runat="server">
                                        <label>Deposite Bnak</label>
                                        <asp:DropDownList ID="ddl_BankName" runat="server" class="form-control" placeholder="Select Deposite Bank" AutoPostBack="True">
                                        </asp:DropDownList>

                                    </div>

                                    <div id="td_BranchAcc" runat="server" visible="false">

                                        <div class="col-md-3 col-xs-6">
                                            <label>Deposite Branch</label>
                                            <asp:DropDownList ID="ddl_Banch" class="form-control" placeholder="Select Deposite Branch" runat="server">
                                            </asp:DropDownList>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Deposite A/C No.</label>
                                            <asp:DropDownList ID="ddl_Account" class="form-control" placeholder="Enter A/C No" runat="server">
                                            </asp:DropDownList>
                                        </div>

                                    </div>


                                    <div id="div_Bankinfo" runat="server" visible="false">
                                        <div id="td_BACode" runat="server" visible="false" class="fltdtls hid">
                                        </div>

                                        <div class="col-md-3 col-xs-6" id="td_BACode1" runat="server" visible="false">
                                            <label>Bank Area Code</label>
                                            <%--<asp:TextBox ID="txt_areacode" placeholder="Bank Area Code" runat="server" class="form-controlaa"></asp:TextBox>--%>
                                            <asp:TextBox ID="txt_areacode" placeholder="Enter Bank Area Code" runat="server" class="form-control"></asp:TextBox>

                                        </div>
                                        <div class="fltdtls hid" id="td_transid" visible="false" runat="server">
                                        </div>

                                        <div class="col-md-3 col-xs-6" id="td_transid1" visible="false" runat="server">
                                            <label>Transaction ID</label>
                                            <asp:TextBox ID="txt_tranid" runat="server" placeholder="Enter Transaction ID" class="form-control"></asp:TextBox>

                                        </div>

                                        <div class="fltdtls hid" width="100px" id="td_BCode" visible="false" runat="server">
                                        </div>

                                        <div class="col-md-3 col-xs-6" id="td_BCode1" visible="false" runat="server">
                                            <label>Branch Code</label>
                                            <asp:TextBox ID="txt_BranchCode" runat="server" placeholder="Enter Branch Code" class="form-control"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div id="tr_Deposite" runat="server" visible="false">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Deposit City</label>
                                            <asp:TextBox ID="txt_city" runat="server" class="form-control" placeholder="Enter Deposit City" onKeyPress="return isChar(event)"></asp:TextBox>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Deposite Date</label>
                                            <asp:TextBox ID="txt_depositedate" runat="server" class="form-control" placeholder="Select Deposite Date"></asp:TextBox>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Deposite Office</label>
                                            <asp:DropDownList ID="ddl_Office" runat="server" class="form-control" placeholder="Deposite Office">
                                            </asp:DropDownList>

                                        </div>
                                        <%-- </div>--%>
                                    </div>
                                    <div id="tr_conper" runat="server" visible="false">
                                        <div class="col-md-3 col-xs-6">
                                            <label>Concern Person</label>
                                            <asp:TextBox ID="txt_concernperson" runat="server" class="form-control" placeholder="Enter Concern Person" onKeyPress="return isChar(event)"></asp:TextBox>

                                        </div>

                                        <div class="col-md-3 col-xs-6">
                                            <label>Receipt No.</label>
                                            <asp:TextBox ID="txt_ReceiptNo" runat="server" class="form-control" placeholder="Receipt No"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-3 col-xs-6">
                                        <label>Reference No.</label>
                                        <asp:TextBox ID="TxtRefNo" runat="server" class="form-control" placeholder="Enter Reference No" MaxLength="30"></asp:TextBox>

                                    </div>

                                    <div class="col-md-3 col-xs-6">
                                        <label>Remark</label>
                                        <asp:TextBox ID="txt_remark" runat="server" class="form-control" placeholder="Remark"></asp:TextBox>

                                    </div>

                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-xs-6">


                                        <asp:Button ID="btn_submitt" Text="Submit" runat="server" CssClass="btn btn-danger" OnClientClick="return ValidateAgent();" />

                                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                    </div>
                                </div>

                                <br />
                            </div>

                            <br />
                            <br />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../Scripts/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../../Scripts/jquery-ui-1.8.8.custom.min.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">

        $(function () {
            $("#ctl00_ContentPlaceHolder1_txt_chequedate").datepicker(
                {
                    numberOfMonths: 1,
                    autoSize: true, dateFormat: 'dd/mm/yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                    changeYear: false, hideIfNoPrevNext: false, maxDate: 0, minDate: '-1y', navigationAsDateFormat: true, defaultDate: +7, showAnim: 'toggle', showOtherMonths: true,
                    selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                }
            ).datepicker("setDate", new Date().getDate - 1);

        });
        $(function () {
            $("#ctl00_ContentPlaceHolder1_txt_depositedate").datepicker(
                {
                    numberOfMonths: 1,

                    autoSize: true, dateFormat: 'dd/mm/yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                    changeYear: false, hideIfNoPrevNext: false, maxDate: 0, minDate: '-1y', navigationAsDateFormat: true, defaultDate: +7, showAnim: 'toggle', showOtherMonths: true,
                    selectOtherMonths: true, showoff: "button", buttonImageOnly: true
                }
            ).datepicker("setDate", new Date().getDate - 1);

        });
    </script>
    <script type="text/javascript">
        $(window).load(function () {
            var hdnAgentId = '<%=hdnAgencyId.ClientID%>'
            var hdnAgentName = '<%=hdnAgencyName.ClientID%>'
           
            document.getElementById("txtAgencyName").value = document.getElementById(hdnAgentName).value;
            document.getElementById("hidtxtAgencyName").value = document.getElementById(hdnAgentId).value;        
        });
    </script>
</asp:Content>

