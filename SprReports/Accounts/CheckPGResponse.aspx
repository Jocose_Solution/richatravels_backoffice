﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckPGResponse.aspx.cs" Inherits="SprReports_Accounts_CheckPGResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<script type="text/javascript">
        function CloseWindow() {
            window.close();
        }
    </script>--%>
    <script type="text/javascript">

        function CloseWindow() {
            window.open('', '_self', '');
            window.close();

        }

    </script>
    <style>
        table.w3-table-all {
            margin: 20px 20px 20px;
        }

        .w3-table-all {
            border: 1px solid #ccc;
        }

        .w3-table, .w3-table-all {
            border-collapse: collapse;
            border-spacing: 0;
            width: 70%;
            display: table;
        }

        html, body {
            font-family: Verdana,sans-serif;
            font-size: 15px;
            line-height: 1.5;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="w3-table-all notranslate">
                <tbody>

                    <tr>
                        <td><b>Order&nbsp;&nbsp;Id:&nbsp;&nbsp;&nbsp;</b><br />
                        </td>
                        <td id="td1" runat="server">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><b>PG&nbsp;&nbsp;Status:&nbsp;&nbsp;&nbsp;</b><br />
                        </td>
                        <td id="tdPgStatus" runat="server">&nbsp;</td>
                    </tr>

                    <tr>
                        <td><b><i>PG&nbsp;&nbsp;Response:&nbsp;&nbsp;&nbsp;</i></b><br />
                        </td>
                        <td id="tdResponse" runat="server"></td>
                    </tr>
                    <tr>
                        <td style="color:red;"><b><i>Note&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</i></b><br /><br />
                        </td>
                        <td style="color:red;">1.Login Payment Gateway websites and Check payment status and payment received or not received in BANK A/C.<br />
                            2.Check  Received  Total Amount.<br /> 
                            3.Check Convenience Fee, Original Amount and Total Amount<br />
                            Convenience Fee + Original Amount=TotalAmount<br />
                            4.Upload only  Original Amount,because Convenience Fee is payment gateway transaction charge<br />
                             (Payment Mode Debit Card,Credit Card ,Net Banking etc.)<br />

                            	
                        </td>
                    </tr>


                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
