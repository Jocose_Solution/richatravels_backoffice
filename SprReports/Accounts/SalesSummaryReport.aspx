﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="SalesSummaryReport.aspx.vb" Inherits="SprReports_Accounts_SalesSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }

        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .viewbutton {
            border: 1px solid #af3e3a;
            padding: 5px;
            border-radius: 5px;
            background: #af3e3a;
            color: #fff;
            font-weight: bold;
            /* text-decoration: none; */
            text-decoration: none;
        }

            .viewbutton:hover {
                text-decoration: none!important;
            }
        #lbOuterContainer,#lbDetailsContainer {
                width: 1200px!important;
        }
        #lbIframe {
            width:100%!important;
        }
    </style>

    <div class="row">

        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Sales Summary Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Executive Id</label>
                                    <asp:TextBox ID="txt_ExecutiveId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 pull-right">
                                <div class="w40 lft">
                                    <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue" Text="Search Result" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <span style="color: #FF0000">* N.B: To get all record without above parameter,do not fill any field, only
                            click on search your booking.</span>
                            </div>
                        </div>
                        <div class="row" id="divReport" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                    <asp:GridView ID="execsummaryrept_grdview" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%" PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Executive ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="ExecutiveID" runat="server" Text='<%#Eval("SalesExecID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Executive Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="ExecutiveName" runat="server" Text='<%#Eval("ExecutiveName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Count">
                                                <ItemTemplate>
                                                    <asp:Label ID="BookingCount" runat="server" Text='<%#Eval("BookingCount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Sale Amount (₹)">
                                                <ItemTemplate>
                                                    <asp:Label ID="totalsale" runat="server" Text='<%#Eval("totalsale")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Due Amount (₹)">
                                                <ItemTemplate>
                                                    <asp:Label ID="totaldue" runat="server" Text='<%#Eval("totaldue")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <a data-toggle="modal" data-target="#myModal" href='SalesSummaryReportByExecID.aspx?ExecutId=<%#Eval("SalesExecID")%> &TransID=' rel="lyteframe"
                                                        rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: 500; color: #004b91">
                                                        <asp:Label ID="OrderID" runat="server" Text='View Detail' CssClass="viewbutton"></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 11px; font-weight: 500; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

