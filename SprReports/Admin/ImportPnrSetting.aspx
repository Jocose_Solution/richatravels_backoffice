﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ImportPnrSetting.aspx.cs" Inherits="SprReports_Admin_ImportPnrSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Import Pnr setting</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                        <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType :</label>
                                    <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" TabIndex="2">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Id :</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                        class="form-control" tabindex="3" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline :</label>
                                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="4" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control" TabIndex="6">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" style="display: none;">
                                    <label for="exampleInputPassword1">Charges :</label>
                                    <asp:TextBox ID="TxtCharges" runat="server" CssClass="form-control" TabIndex="5" placeholder="Hold Booking Charges" Text="0" MaxLength="5" onKeyPress="return keyRestrict(event,'.0123456789');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" CssClass="button buttonBlue" TabIndex="7" OnClientClick="return CheckDeal();" OnClick="BtnSubmit_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-12" style="width: 1100px;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grid_PnrSetting" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered table-striped" GridLines="None" Width="100%" PageSize="30" OnRowDeleting="OnRowDeleting"
                                            AllowPaging="True" OnPageIndexChanging="grid_PnrSetting_PageIndexChanging" OnRowEditing="grid_PnrSetting_RowEditing" OnRowCancelingEdit="grid_PnrSetting_RowCancelingEdit">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Trip Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblTripTypeName" runat="server" Text='<%#Eval("TripTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("AirlineName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Group Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroupType" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Agent Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Charges">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCharges" runat="server" Text='<%#Eval("Charges") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <%--<EditItemTemplate>
                                                        <asp:TextBox ID="txt_Charges" runat="server" Text='<%#Eval("Charges") %>' Width="60px"
                                                            onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="5" BackColor="#ffff66"></asp:TextBox>
                                                    </EditItemTemplate>--%>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#Eval("IsActive") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <%--<EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_IsActive" runat="server" Width="150px" DataValueField='<%#Eval("IsActive")%>' SelectedValue='<%#Eval("IsActive")%>'>
                                                            <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>--%>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update">
                                                    <ItemTemplate>
                                                        <a href='UpdateImportPnrSetting.aspx?ID=<%#Eval("ID")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <u>Update</u>
                                                        </a>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function AppliedOn() {
            //var DealType = $("#ctl00_ContentPlaceHolder1_DdlDealCodeType").val();
            //if (DealType == "PF") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").hide();
            //}
            //else if (DealType == "PC") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").show();
            //}
            //else {
            //    $("#DivAppliedOn").hide();
            //    $("#DivFareType").hide();
            //}
        }

        function CheckDeal() {
            if ($("#ctl00_ContentPlaceHolder1_TxtCharges").val() == "") {
                alert("Enter hold booking charges :.");
                $("#ctl00_ContentPlaceHolder1_TxtCharges").focus();
                return false;
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>
</asp:Content>

