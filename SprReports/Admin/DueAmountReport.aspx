﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="DueAmountReport.aspx.cs" Inherits="SprReports_Admin_DueAmountReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Due Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" id="td_Agency" runat="server">
                                    <label for="exampleInputPassword1">
                                        Agency Name
                                    </label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                            <div class="col-md-4" id="divFromDate" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4" id="divToDate" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Sales Person</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlSalesPerson" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fixed Credit Limit</label>
                                    <asp:TextBox ID="txtFixedCreditLimit" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>
                                        Temp Credit Limit
                                    </label>
                                    <asp:TextBox ID="txtTempCreditLimit" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" OnClick="btn_result_Click" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" OnClick="btn_export_Click"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive text-nowrap">
                                    <table class="table" data-toggle="table">
                                        <asp:ListView ID="lstDueAmount" runat="server" OnPagePropertiesChanged="lstDueAmount_PagePropertiesChanged">
                                            <LayoutTemplate>
                                                <tr>
                                                    <th>UserID</th>
                                                    <th>AgencyID</th>
                                                    <th>Agency Name</th>
                                                    <th>Mobile No</th>
                                                    <th>Email ID</th>
                                                    <th>Sales Person</th>
                                                    <th>Due Amount</th>
                                                    <th>Balance</th>
                                                    <th>Available Limit</th>
                                                    <th>Temp Credit</th>
                                                    <th>Given Date</th>
                                                    <th>Fixed Credit</th>
                                                    <th>Given Date</th>
                                                    <th>Transaction Date</th>
                                                    <th>No Of Days Since Pending</th>
                                                </tr>
                                                <div runat="server" id="ItemPlaceholder"></div>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("User_Id") %></td>
                                                    <td><%# Eval("AgencyId") %></td>
                                                    <td><%# !string.IsNullOrEmpty(Eval("Agency_Name").ToString())?Eval("Agency_Name"):"- - -" %></td>
                                                    <td><%# Eval("Mobile") %></td>
                                                    <td><%# Eval("Email") %></td>
                                                    <td><%# !string.IsNullOrEmpty(Eval("SalesPerson").ToString())?Eval("SalesPerson"): "- - -" %></td>
                                                    <td><%# Eval("DueAmount").ToString().Replace(".0000",".00") %></td>
                                                    <td><%# Eval("Balance").ToString().Replace(".0000",".00") %></td>
                                                    <td><%# Eval("AgentLimit").ToString().Replace(".0000",".00") %></td>
                                                    <td><%# Eval("TempLimit").ToString().Replace(".0000",".00") %></td>
                                                    <td><%# Eval("TempLimitTrnsDate") %></td>
                                                    <td><%# Eval("FixedLimit").ToString().Replace(".0000",".00") %></td>
                                                    <td><%# Eval("FixedLimitTrnsDate") %></td>
                                                    <td><%# Eval("Crd_Trns_Date") %></td>
                                                    <td>--</td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <table class="table" data-toggle="table">
                                                    <tr>
                                                        <th>UserID</th>
                                                        <th>AgencyID</th>
                                                        <th>Agency Name</th>
                                                        <th>Mobile No</th>
                                                        <th>Email ID</th>
                                                        <th>Sales Person</th>
                                                        <th>Due Amount</th>
                                                        <th>Balance</th>
                                                        <th>Available Limit</th>
                                                        <th>Temp Credit</th>
                                                        <th>Given Date</th>
                                                        <th>Fixed Credit</th>
                                                        <th>Given Date</th>
                                                        <th>Transaction Date</th>
                                                        <th>No Of Days Since Pending</th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="15" style="color: red; text-align: center;">NO RECORD FOUND !</td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                    </table>
                                    <asp:DataPager runat="server" ID="PagingButtom" PagedControlID="lstDueAmount" PageSize="20">
                                        <Fields>
                                            <asp:NextPreviousPagerField FirstPageText="First" PreviousPageText="&laquo;" ShowFirstPageButton="false" ShowNextPageButton="false" ShowPreviousPageButton="true" RenderDisabledButtonsAsLabels="false" />
                                            <asp:NumericPagerField ButtonCount="10" />
                                            <asp:NextPreviousPagerField LastPageText="Last" NextPageText="&raquo;" ShowLastPageButton="false" ShowNextPageButton="true" ShowPreviousPageButton="false" RenderDisabledButtonsAsLabels="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

