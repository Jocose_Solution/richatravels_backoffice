﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Admin_TicketingCrd : System.Web.UI.Page
{
    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
            //if (Convert.ToString(Session["User_Type"]).ToUpper() != "ADMIN")
            //{
            //    Response.Redirect("~/Login.aspx");
            //}      

            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch(Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region Insert
            string Provider = Convert.ToString(DdlProvider.SelectedValue);
            if(Provider=="0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select provider!!');", true);
                return;
            }
            string Airline = Convert.ToString(Request["hidtxtAirline"]);
            string FlightName = Convert.ToString(Request["txtAirline"]);
            string AirCode = "";
            string AirlineName = "";

            if (!string.IsNullOrEmpty(FlightName))
            {
                if (!string.IsNullOrEmpty(Airline))
                {
                    AirlineName = Airline.Split(',')[0];
                    if (Airline.Split(',').Length > 1)
                    {
                        AirCode = Airline.Split(',')[1];
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                        return;
                    }
                }
            }
            else
            {
                AirlineName = "ALL";
                AirCode = "ALL";
            }           
            string Trip = Convert.ToString(DdlTripType.SelectedValue);
            string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
            string OnlineTkt = Convert.ToString(DdlStatus.SelectedValue);
            string CrpId=TxtCorporateID.Text;
            string USERID=TxtUserID.Text;
            string PWD=TxtPassword.Text;
            string PCC=TxtProviderPcc.Text;
            string QNO=TxtPnrQNo.Text;

            string TicketThrough = DdlTicketThrough.SelectedValue;
            if (TicketThrough == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select Ticketing api!!');", true);
                return;
            }
            
            string QueuePCC = TxtQueuePCC.Text;
            string QNOForAPI = TxtQNOForAPI.Text;
            string ForceToHold = Convert.ToString(DrpForceToHold.SelectedValue);
            string CrdType = Convert.ToString(DdlCrdType.SelectedValue);
            string ActionType = "insert";
            if (Provider == "1G")
            {
                TicketThrough = "";
                QueuePCC = "";
                QNOForAPI = "";
            }
            msgout = "";
            int flag = TicketingCredential(0, CrpId, USERID, PWD, PCC, QNO, AirCode, OnlineTkt, Trip, Provider, TicketThrough, QueuePCC, QNOForAPI, ForceToHold, ActionType, CrdType);
           
            if (flag > 0)
            {
                TxtCorporateID.Text="";
                TxtUserID.Text = "";
                TxtPassword.Text = "";
                TxtProviderPcc.Text = "";
                TxtPnrQNo.Text = "";
                DdlProvider.SelectedValue="0";
                TxtQueuePCC.Text = "";
                TxtQNOForAPI.Text = "";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='DealCodeMaster.aspx'; ", true);                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                BindGrid();
            }
            else
            {
                if (msgout == "EXISTS")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Already exists,Please update..');", true);
                    BindGrid();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);                   
                }       
                
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='TicketingCrd.aspx'; ", true);
            return;
        }

    }
    private int TicketingCredential(int Counter, string CrpId, string USERID, string PWD, string PCC, string QNO, string AIRLINE, string OnlineTkt, string Trip, string Provider, string TicketThrough, string QueuePCC, string QNOForAPI, string ForceToHold, string ActionType, string CrdType)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        //string ActionType = "insert";
        try
        {            
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SpTicketingCredential", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@HAP", CrpId);
            cmd.Parameters.AddWithValue("@USERID", USERID);
            cmd.Parameters.AddWithValue("@PWD", PWD);
            cmd.Parameters.AddWithValue("@PCC", PCC);
            cmd.Parameters.AddWithValue("@QNO", QNO);
            cmd.Parameters.AddWithValue("@AIRLINE", AIRLINE);
            cmd.Parameters.AddWithValue("@OnlineTkt", Convert.ToBoolean(OnlineTkt));
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@TicketThrough", TicketThrough);
            cmd.Parameters.AddWithValue("@QueuePCC", QueuePCC);
            cmd.Parameters.AddWithValue("@QNOForAPI", QNOForAPI);            
            cmd.Parameters.AddWithValue("@ForceToHold", Convert.ToBoolean(ForceToHold));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@CrdType", CrdType);
            cmd.Parameters.AddWithValue("@Projecttype", ddProject.SelectedValue.Trim());
            
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;

            if (con.State == System.Data.ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();

        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }

    public void BindGrid()
    {
        try
        {
            Grid1.DataSource = GetRecord();
            Grid1.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            adap = new SqlDataAdapter("SpTicketingCredential", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;            
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.AddWithValue("@Projecttype", ddProject.SelectedValue.Trim());
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void Grid1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void Grid1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void Grid1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {            
            int result = 0;
            Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
            int Counter = Convert.ToInt16(lblSNo.Text.Trim().ToString());

            DropDownList ddlGrdProvider = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdProvider");
            string Provider = ddlGrdProvider.SelectedValue;

            TextBox txtGrdCorporateId = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdCorporateId"));
            string CrpId = Convert.ToString(txtGrdCorporateId.Text);
            if (string.IsNullOrEmpty(CrpId))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Corporate Id!');", true);
                return;
            }

            TextBox txtGrdUserId = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdUserId"));
            string USERID = Convert.ToString(txtGrdUserId.Text);

            if (string.IsNullOrEmpty(USERID))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter UserId!');", true);
                return;
            }            
            TextBox txtGrdPWD = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdPWD"));
            string PWD = Convert.ToString(txtGrdPWD.Text);

            if (string.IsNullOrEmpty(PWD))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter password!');", true);
                return;
            }  

            TextBox txtGrdPCC = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdPCC"));
            string PCC = Convert.ToString(txtGrdPCC.Text);

            if (string.IsNullOrEmpty(PCC))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter PCC!');", true);
                return;
            }  

            TextBox txtGrdQNO = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdQNO"));
            string QNO = Convert.ToString(txtGrdQNO.Text);

            if (string.IsNullOrEmpty(PCC))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter PCC!');", true);
                return;
            }              
            string QueuePCC = "";
            string QNOForAPI = "";

            //TextBox txtGrdTicketThrough = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdTicketThrough"));
            //TicketThrough = Convert.ToString(txtGrdTicketThrough.Text);
            DropDownList ddlGrdTicketThrough = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdTicketThrough");
            string TicketThrough = ddlGrdTicketThrough.SelectedValue;
            if (string.IsNullOrEmpty(TicketThrough))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Select Ticketing Api!');", true);
                return;
            }
            if (TicketThrough != "1G")
            {                
                TextBox txtGrdQueuePCC = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdQueuePCC"));
                QueuePCC = Convert.ToString(txtGrdQueuePCC.Text);
                if (string.IsNullOrEmpty(QueuePCC))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Provider Queue PCC!');", true);
                    return;
                }  
                TextBox txtGrdQNOForAPI = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdQNOForAPI"));
                QNOForAPI = Convert.ToString(txtGrdQNOForAPI.Text);
                if (string.IsNullOrEmpty(QNOForAPI))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter QNO For API!');", true);
                    return;
                }  
            }

            DropDownList ddlGrdForceToHold = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdForceToHold");
           // string FToHold = ddlGrdForceToHold.SelectedValue;
            string ForceToHold = ddlGrdForceToHold.SelectedValue;//"true";
            //if (FToHold == "0")
            //{
            //    ForceToHold = "false";
            //}
            DropDownList ddlGrdOnlineTkt = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdOnlineTkt");
            //string Online = ddlGrdOnlineTkt.SelectedValue;
            string OnlineTkt = ddlGrdOnlineTkt.SelectedValue;
            //if (Online == "0")
            //{
            //    OnlineTkt = "false";
            //}
            string CrdType = "";
            string ActionType = "GRIDUPDATE";
            result = TicketingCredential(Counter, CrpId, USERID, PWD, PCC, QNO, "", OnlineTkt, "", Provider, TicketThrough, QueuePCC, QNOForAPI, ForceToHold, ActionType, CrdType);           

            if (result > 0)
            {
                Grid1.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SpTicketingCredential", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid1.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
}