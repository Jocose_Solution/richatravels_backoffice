﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FlightSummary.aspx.vb" Inherits="SprReports_API_FLIGHT_FlightSummary" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="css/transtour.css" rel="stylesheet" type="text/css" />
    <link href="css/core_style.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/itz.css" rel="stylesheet" />   
    <script type="text/javascript" language='javascript'>
        function callprint(strid) {
            var prtContent = $('#' + strid);
            var sst = '<html><head><title>Ticket Details</title><link rel="stylesheet" href="http://richatravels.com/CSS/itz.css" type="text/css" media="print"></style></head><body>';
            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write('<html><head><title>Ticket Details</title>');
            WinPrint.document.write('</head><body>' + prtContent.html() + '</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
    <style type="text/css">
        .style1 {
            height: 14px;
        }
        .style2 {
            width: 40%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divtkt" runat="server" style="margin: 5px auto; width: 90%; background-color: #FFFFFF; padding: 5px;">
            <div class="large-12 medium-12 small-12">
                <div id="divprint" runat="server" style="margin: 5px auto; border: 1px #20313f solid; width: 90%; background-color: #FFFFFF; padding: 5px;">
                    <div id="div_mail" runat ="server">
                        <asp:Label ID="LabelTkt" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
       <div id="Div_Main" runat="server">
            <div style="margin: 5px auto; width: 90%; background-color: #FFFFFF; padding: 5px;">
            <div class="large-12 medium-12 small-12" style="border: thin solid #004b91; background-color: #E9E9E9;" bgcolor="White">
                <div style="font-family: arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color: #004b91; padding-left: 10px; padding-top: 5px; padding-bottom: 5px;"
                    id="td_showaddchage">
                    <input type="button" id="btn_addcharge" name="btn_addcharge" value="AddServiceCharge"
                        style="font-weight: bold; background-color: #004b91; color: #FFFFFF; font-family: arial, Helvetica, sans-serif; font-size: 13px"
                        onclick="showcharge();" />&nbsp;&nbsp;<br />
&nbsp; <span style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #FF3300"><b style="font-family: arial, Helvetica, sans-serif; font-size: 13px; color: #004b91; font-weight: bold">Note:</b>&nbsp; We are not
                                        storing any data regarding additional charge into our database.</span>
                </div>
                <div id="td_servicecharge" style="display: none">
                    <div class="large-2 medium-2 small-4">
                        Select Charge Type
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <asp:DropDownList ID="ddl_srvtype" runat="server">
                            <asp:ListItem Value="">--Select--</asp:ListItem>
                            <asp:ListItem Value="TC">Transaction Charge</asp:ListItem>
                            <asp:ListItem Value="TAX">Tax</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="large-2 medium-2 small-4 large-push-1 medium-push-1">
                        Charge Amount
                    </div>
                    <div class="large-2 medium-2 small-4 large-push-2 medium-push-2">
                        <input type="text" id="txt_srvcharge" name="txt_srvcharge" runat="server" onkeypress="return NumericOnly(event);" />
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <a onclick="hidecharge();" href="#">
                            <img src="../Images/close.png" align="right" alt="Close" /></a>
                    </div>
                    <div class="clear"></div>

                    <div class="large-2 medium-2 small-6 large-push-10 medium-push-10 small-push-10">
                        <input type="button" id="btn_edit" name="btn_edit" value="Add Charge" onclick="AdditionalCharge();"
                            style="font-weight: bold; background-color: #004b91; color: #FFFFFF; font-family: arial, Helvetica, sans-serif; font-size: 13px;width:120px;" />
                    </div>

                    <div>
                        <b>NOTE:</b>&nbsp; Charge amount should be per pax
                    </div>

                    <div>
                        <%--For TC --%>
                        <input type="hidden" id="hidtcadt" name="hidtcadt" />
                        <input type="hidden" id="hidtcchd" name="hidtcchd" />

                        <input type="hidden" id="hidtotadt" name="hidtotadt" />
                        <input type="hidden" id="hidtotchd" name="hidtotchd" />

                        <input type="hidden" id="hidgrandtot" name="hidgrandtot" />
                        <input type="hidden" id="hidfinaltot" name="hidfinaltot" />

                        <%--For Tax --%>
                        <input type="hidden" id="hidtaxadt" name="hidtaxadt" />
                        <input type="hidden" id="hidtaxchd" name="hidtaxchd" />

                        <input type="hidden" id="hidtaxtotadt" name="hidtaxtotadt" />
                        <input type="hidden" id="hidtaxtotchd" name="hidtaxtotchd" />

                        <input type="hidden" id="hidtaxgrandtot" name="hidtaxgrandtot" />
                        <input type="hidden" id="hidtaxfinaltot" name="hidtaxfinaltot" />

                        <input type="hidden" id="hedtotInfant" name="hedtotInfant" />
                        <input type="hidden" id="hedFinalTotal" name="hedFinalTotal" />
                        <input type="hidden" id="hedFinalTotaltax" name="hedFinalTotaltax" />

                        <%--Pax Wise--%>

                        <input type="hidden" id="hidperpaxtc" name="hidperpaxtc" />
                        <input type="hidden" id="hidperpaxTCtot" name="hidperpaxTCtot" /> 
                         <input type="hidden" id="hidperpaxgrandTCtot" name="hidperpaxgrandtot" />

                        <input type="hidden" id="hidperpaxtax" name="hidperpaxtax" />
                        <input type="hidden" id="hidperpaxTaxtot" name="hidperpaxTaxtot" />
                        <input type="hidden" id="hidperpaxgrandTaxtot" name="hidperpaxgrandtot" />
                    </div>
                </div>
            </div>
            <div>
                <input type="hidden" id="Hidden1" runat="server" name="Hidden1" />
            </div>
        </div>
        <div id="div1" runat="server" style="margin: 5px auto; border: 1px #20313f solid; width: 90%; background-color: #FFFFFF; padding: 5px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="width: 10%"></td>
                    <td style="width: 80%" bgcolor="White">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" style="padding-left: 10px" width="300px">
                                    <a href='javascript:;' onclick='javascript:callprint("divprint");'>
                                        <img src='../Images/print_booking.jpg' border='0' alt="" /></a>
                                </td>
                                <td width="150px">
                                    <asp:Button ID="btn_exporttoword" runat="server" Text="ExportToWord" BackColor="#004b91"
                                        Font-Bold="False" ForeColor="White" CausesValidation="False" OnClientClick="return hidecharge();" />
                                </td>
                                <td>
                                    <asp:Button ID="btn_exporttoecel" runat="server" Text="ExportToExcel" BackColor="#004b91"
                                        Font-Bold="False" ForeColor="White" CausesValidation="False" OnClientClick="return hidecharge();"
                                        Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 10%"></td>
                </tr>
            </table>
        </div>
        <div style="margin: 10px auto; border: 1px #20313f solid; width: 90%; background-color: #FFFFFF; padding: 10px;">
            <table width="60%" border="0" cellspacing="2" cellpadding="2" bgcolor="#20313f" style="height: 80px"
                align="center">
                <tr>
                    <td colspan="2" style="color: #ffffff; font-size: 12px;">
                        <strong style="padding-left: 10px">Send E-Mail:</strong>
                    </td>
                </tr>
                <tr>
                    <td style="color: #ffffff; font-size: 12px; padding-left: 15px;" valign="top" class="style2">Email-ID :
                    <asp:TextBox ID="txt_email" runat="server" CssClass="textboxflight"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txt_email"
                            ErrorMessage="*" ForeColor="#990000" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <br />
                        <div style="text-align: left; color: #EC2F2F">
                            <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txt_email"
                                ValidationExpression=".*@.*\..*" ErrorMessage="*Invalid E-Mail ID." Display="dynamic">*Invalid E-Mail ID.</asp:RegularExpressionValidator>
                        </div>
                    </td>
                    <td width="60%" valign="top">
                        <asp:Button ID="btn" runat="server" Text="Send"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="color: red; font-size: 12px; padding-left: 15px;">
                        <asp:Label ID="mailmsg" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
       </div>
       
    </form>
</body>
</html>

<script src="../Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    function NumericOnly(event) {
        var charCode = (event.keyCode ? event.keyCode : event.which);
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    function showcharge() {
        $("#td_servicecharge").show();
        $("#td_showaddchage").hide();
    }
    function hidecharge() {
        $("#td_servicecharge").hide();
        $("#td_showaddchage").show();
    }
    function AdditionalCharge() {
        if ($("#ddl_srvtype").val() == "") {
            alert('Please select charge type');
            $("#ddl_srvtype").focus();
            return false;
        }
        if ($("#txt_srvcharge").val() == "") {
            alert('Please fill charge amount');
            $("#txt_srvcharge").focus();
            return false;
        }
        //Get Query string 
        var collection = {}; var k = 0;
        var pgUrl = window.location.search.substring(1);
        var qarray = pgUrl.split('&');
        for (var i = 0; i <= qarray.length - 1; i++) {
            var splt = qarray[i].split('=');
            if (splt.length > 0) {
                for (var j = 0; j < splt.length - 1; j++) {
                    collection[k] = splt[j + 1];
                }
                k += 1;
            }
        }
        if (collection[1] == "") {
            //Claculation for whole order

            var SrvCharge = $("#txt_srvcharge").val();
            var SrvType = $("#ddl_srvtype").val();
            var adtcnt = $("#td_adtcnt")[0].innerHTML;
            var chdcnt = 0;
            var tcadt = 0, tcadttot = 0, tcchd = 0, tcchdtot = 0;
            var taxadt = 0, taxadttot = 0, taxchd = 0, taxchdtot = 0, TotalInfant = 0, FinalTotal = 0;;

            if ($('#td_chdcnt').length > 0) {
                chdcnt = $("#td_chdcnt")[0].innerHTML;
            }
            if (SrvType == "TC") {

                if ($("#hedFinalTotal").val() != "") {
                    $("#td_grandtot")[0].innerHTML = parseInt($("#hedFinalTotal").val());
                }
                if ($("#hedFinalTotal").val() == "") {

                    FinalTotal = $("#hedFinalTotal").val($("#td_grandtot")[0].innerHTML).val();
                }
                else {
                    FinalTotal = $("#hedFinalTotal").val();
                }
                if (adtcnt > 0) {
                    //Checking hidden field for tax
                    if ($("#hidtcadt").val() != "") {
                        $("#td_tcadt")[0].innerHTML = parseInt($("#hidtcadt").val());
                    }
                    if ($("#hidtotadt").val() != "") {
                        $("#td_adttot")[0].innerHTML = parseInt($("#hidtotadt").val());
                    }
                    if ($("#hidtcadt").val() == "") {
                        tcadt = $("#hidtcadt").val($("#td_tcadt")[0].innerHTML).val();
                    }
                    else {
                        tcadt = $("#hidtcadt").val();
                    }

                    if ($("#hidtotadt").val() == "") {
                        tcadttot = $("#hidtotadt").val($("#td_adttot")[0].innerHTML).val();
                    }
                    else {
                        tcadttot = $("#hidtotadt").val();
                    }
                    $("#td_tcadt")[0].innerHTML = parseInt(tcadt) + (parseInt(SrvCharge) * parseInt(adtcnt));
                    $("#td_adttot")[0].innerHTML = parseInt(tcadttot) + (parseInt(SrvCharge) * parseInt(adtcnt));
                }
                if (chdcnt > 0) {
                    //For CHD TC

                    if ($("#hidtcchd").val() != "") {
                        $("#td_tcchd")[0].innerHTML = parseInt($("#hidtcchd").val());
                    }

                    if ($("#hidtotchd").val() != "") {
                        $("#td_chdtot")[0].innerHTML = parseInt($("#hidtotchd").val());
                    }

                    if ($("#hidtcchd").val() == "") {
                        tcchd = $("#hidtcchd").val($("#td_tcchd")[0].innerHTML).val();
                    }

                    else {
                        tcchd = $("#hidtcchd").val();
                    }

                    if ($("#hidtotchd").val() == "") {
                        tcchdtot = $("#hidtotchd").val($("#td_chdtot")[0].innerHTML).val();
                    }
                    else {
                        tcchdtot = $("#hidtotchd").val();
                    }

                    $("#td_tcchd")[0].innerHTML = parseInt(tcchd) + (parseInt(SrvCharge) * parseInt(chdcnt));
                    $("#td_chdtot")[0].innerHTML = parseInt(tcchdtot) + (parseInt(SrvCharge) * parseInt(chdcnt));
                }

                $("#td_grandtot")[0].innerHTML = parseInt(FinalTotal) + (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
            }

            if (SrvType == "TAX") {

                if ($("#hedFinalTotaltax").val() != "") {
                    $("#td_grandtot")[0].innerHTML = parseInt($("#hedFinalTotaltax").val());
                }
                if ($("#hedFinalTotaltax").val() == "") {

                    FinalTotal = $("#hedFinalTotaltax").val($("#td_grandtot")[0].innerHTML).val();
                }
                else {
                    FinalTotal = $("#hedFinalTotaltax").val();
                }
                if (adtcnt > 0) {
                    if ($("#hidtaxadt").val() != "") {
                        $("#td_taxadt")[0].innerHTML = parseInt($("#hidtaxadt").val());
                    }
                    if ($("#hidtaxtotadt").val() != "") {
                        $("#td_adttot")[0].innerHTML = parseInt($("#hidtaxtotadt").val());
                    }
                    //For Adult TAX
                    if ($("#hidtaxadt").val() == "") {
                        taxadt = $("#hidtaxadt").val($("#td_taxadt")[0].innerHTML).val();
                    }
                    else {
                        taxadt = $("#hidtaxadt").val();
                    }
                    if ($("#hidtaxtotadt").val() == "") {
                        taxadttot = $("#hidtaxtotadt").val($("#td_adttot")[0].innerHTML).val();
                    }
                    else {
                        taxadttot = $("#hidtaxtotadt").val();
                    }
                    $("#td_taxadt")[0].innerHTML = parseInt(taxadt) + (parseInt(SrvCharge) * parseInt(adtcnt));
                    $("#td_adttot")[0].innerHTML = parseInt(taxadttot) + (parseInt(SrvCharge) * parseInt(adtcnt));

                }
                if (chdcnt > 0) {
                    if ($("#hidtaxchd").val() != "") {
                        $("#td_taxchd")[0].innerHTML = parseInt($("#hidtaxchd").val());
                    }

                    if ($("#hidtaxtotchd").val() != "") {
                        $("#td_chdtot")[0].innerHTML = parseInt($("#hidtaxtotchd").val());
                    }

                    if ($("#hidtaxgrandtot").val() != "") {
                        $("#td_grandtot")[0].innerHTML = parseInt($("#hidtaxgrandtot").val());
                    }

                    //For Child TAX
                    if ($("#hidtaxchd").val() == "") {
                        taxchd = $("#hidtaxchd").val($("#td_taxchd")[0].innerHTML).val();
                    }

                    else {
                        taxchd = $("#hidtaxchd").val();
                    }

                    if ($("#hidtaxtotchd").val() == "") {
                        taxchdtot = $("#hidtaxtotchd").val($("#td_chdtot")[0].innerHTML).val();
                    }

                    else {
                        taxchdtot = $("#hidtaxtotchd").val();
                    }

                    $("#td_taxchd")[0].innerHTML = parseInt(taxchd) + (parseInt(SrvCharge) * parseInt(chdcnt));
                    $("#td_chdtot")[0].innerHTML = parseInt(taxchdtot) + (parseInt(SrvCharge) * parseInt(chdcnt));
                }
                $("#td_grandtot")[0].innerHTML = parseInt(FinalTotal) + (parseInt(SrvCharge) * parseInt(adtcnt)) + (parseInt(SrvCharge) * parseInt(chdcnt));
            }
            //$("#td_tcadt").focus();
            alert('Fare summary changed sucessfully.');
        }
        else {
            //Calculation by pax id
            var SrvCharge = $("#txt_srvcharge").val();
            var SrvType = $("#ddl_srvtype").val();
            var tcperpax = 0, tcpaxTotal = 0, perpaxgrandtot = 0;
            var taxperpax = 0;
            var paxtype = $("#td_perpaxtype")[0].innerHTML;
            if (paxtype == "INF") {
                alert('Fare will not change for Infant');
                return false;
            }
            if (SrvType == "TC" && paxtype != "INF") {

                if ($("#hidperpaxtc").val() != "") {
                    $("#td_perpaxtc")[0].innerHTML = parseInt($("#hidperpaxtc").val());
                }
                if ($("#hidperpaxTCtot").val() != "") {
                    $("#td_totalfare")[0].innerHTML = parseInt($("#hidperpaxTCtot").val());
                }
                if ($("#hidperpaxgrandTCtot").val() != "") {
                    $("#td_grandtot")[0].innerHTML = parseInt($("#hidperpaxgrandTCtot").val());
                }
                if ($("#hidperpaxtc").val() == "") {
                    tcperpax = $("#hidperpaxtc").val($("#td_perpaxtc")[0].innerHTML).val();
                }
                else {
                    tcperpax = $("#hidperpaxtc").val();
                }
                if ($("#hidperpaxTCtot").val() == "") {
                    tcpaxTotal = $("#hidperpaxTCtot").val($("#td_totalfare")[0].innerHTML).val();
                }
                else {
                    tcpaxTotal = $("#hidperpaxTCtot").val();
                }
                if ($("#hidperpaxgrandTCtot").val() == "") {
                    perpaxgrandtot = $("#hidperpaxgrandTCtot").val($("#td_grandtot")[0].innerHTML).val();
                }
                else {
                    perpaxgrandtot = $("#hidperpaxgrandTCtot").val();
                }
                $("#td_perpaxtc")[0].innerHTML = parseInt(tcperpax) + (parseInt(SrvCharge));
                $("#td_totalfare")[0].innerHTML = parseInt(tcpaxTotal) + (parseInt(SrvCharge));
                $("#td_grandtot")[0].innerHTML = parseInt(perpaxgrandtot) + (parseInt(SrvCharge));
            }
            if (SrvType == "TAX" && paxtype != "INF") {

                if ($("#hidperpaxtax").val() != "") {
                    $("#td_perpaxtax")[0].innerHTML = parseInt($("#hidperpaxtax").val());
                }
                if ($("#hidperpaxTaxtot").val() != "") {
                    $("#td_totalfare")[0].innerHTML = parseInt($("#hidperpaxTaxtot").val());
                }
                if ($("#hidperpaxgrandTaxtot").val() != "") {
                    $("#td_grandtot")[0].innerHTML = parseInt($("#hidperpaxgrandTaxtot").val());
                }
                if ($("#hidperpaxtax").val() == "") {
                    taxperpax = $("#hidperpaxtax").val($("#td_perpaxtax")[0].innerHTML).val();
                }
                else {
                    taxperpax = $("#hidperpaxtax").val();
                }
                if ($("#hidperpaxTaxtot").val() == "") {
                    tcpaxTotal = $("#hidperpaxTaxtot").val($("#td_totalfare")[0].innerHTML).val();
                }
                else {
                    tcpaxTotal = $("#hidperpaxTaxtot").val();
                }
                if ($("#hidperpaxgrandTaxtot").val() == "") {
                    perpaxgrandtot = $("#hidperpaxgrandTaxtot").val($("#td_grandtot")[0].innerHTML).val();
                }
                else {
                    perpaxgrandtot = $("#hidperpaxgrandTaxtot").val();
                }
                $("#td_perpaxtax")[0].innerHTML = parseInt(taxperpax) + (parseInt(SrvCharge));
                $("#td_totalfare")[0].innerHTML = parseInt(tcpaxTotal) + (parseInt(SrvCharge));
                $("#td_grandtot")[0].innerHTML = parseInt(perpaxgrandtot) + (parseInt(SrvCharge));
            }
            alert('Fare summary changed sucessfully.');
            // $("#td_perpaxtax").focus();
        }
        $("#Hidden1").val($("#div_mail")[0].innerHTML);
    }
</script>
