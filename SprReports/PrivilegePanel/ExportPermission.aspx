﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ExportPermission.aspx.cs" Inherits="ExportPermission" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    
      <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
     

  

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
   <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PrivilegePanel > Export Permission</h3>
                    </div>
                    <div class="panel-body">
                           <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                             <div class="form-group">
                                     <label id="useridlb">UserID :</label>
                                      <asp:DropDownList ID="UserID" runat="server" Width="150px" Height="30px" >
                                        </asp:DropDownList>
                                 </div>
                                        </div>
                                    </div>

                     

                                   <div class="col-md-4">
                                   <div class="form-group">
                                  <label id="ddlRole_typelb">Role:</label>
                                     <asp:DropDownList  ID="ddlRole_type" runat="server" Width="150px" Height="30px"  >
                                     </asp:DropDownList>
                                 </div>
                                    </div>


                                 <div class="col-md-4">
                                         <div class="form-group">
                                       <label id="StatusTypelb">Status :</label>
                                      <asp:DropDownList ID="StatusType" runat="server" Width="150px" Height="30px"  >
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>
                                 </div>
                                    </div>
                                                             <div class="col-md-4">
                                         <div class="form-group">
                                       <label id="StatusTypelbedit">Edit Status :</label>
                                      <asp:DropDownList ID="ddleditstatus" runat="server" Width="150px" Height="30px"  >
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>
                                 </div>
                                    </div>
                                        <div class="col-md-4">
                                         <div class="form-group">
                                       <label id="StatusTypelbprint">Print Status :</label>
                                      <asp:DropDownList ID="ddlprintstatus" runat="server" Width="150px" Height="30px"  >
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>
                                 </div>
                                    </div>


                                </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button buttonBlue" OnClick="Submit_Click" ValidationGroup="group1" />

                                    <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>

                                </div>
                            </div>
                        </div>


                       <%-- <div class="form-group">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="button buttonBlue" OnClick="Cancel_Click" />

                            </div>
                        </div>--%>
                           <%--<div class="row">
                               <div class="col-md-8">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                        </div>
                                   </div>
                               </div>--%>

                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                            OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                            OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                            CssClass="table" GridLines="Both" Width="100%">

                            <Columns>
                                <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("UserID") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                   <asp:TemplateField HeaderText="RoleType" ItemStyle-Width="150" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleType" runat="server" Text='<%# Eval("RoleType") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                   

                                   <asp:TemplateField HeaderText="CreatedDate" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>


                                    <asp:TemplateField HeaderText="CreatedBy" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedBy") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                                         
                                   <asp:TemplateField HeaderText="UpdateDate" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("UpdateDate") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                
                                   <asp:TemplateField HeaderText="UpdateBy" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUpdateBy" runat="server" Text='<%# Eval("UpdateBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                              <%--  <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Name" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleNameHidden" runat="server" Text='<%# Eval("Role") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>
                                </asp:TemplateField>--%>

        
                                <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>

                                        <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>

                                    </EditItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit Status" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lbleditstatus" runat="server" Text='<%# GetStatusVal( Eval("Editstatus")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>

                                        <asp:DropDownList ID="ddleditstatus" runat="server" Width="150px" SelectedValue='<%# Eval("Editstatus").ToString() %>'>
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>

                                    </EditItemTemplate>

                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Print Status" ItemStyle-Width="150">
                                    <ItemTemplate>
                                        <asp:Label ID="lblprintstatus" runat="server" Text='<%# GetStatusVal( Eval("Printstatus")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>

                                        <asp:DropDownList ID="ddlprintstatus" runat="server" Width="150px" SelectedValue='<%# Eval("Printstatus").ToString() %>'>
                                            <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                            <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                        </asp:DropDownList>

                                    </EditItemTemplate>

                                </asp:TemplateField>

                              
                                   <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                   <EditItemTemplate>
                                <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update" ></asp:LinkButton>
                                <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                  </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>


                            </Columns>

                        </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>



</asp:Content>

