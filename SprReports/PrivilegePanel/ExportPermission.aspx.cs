﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class ExportPermission : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])) || string.IsNullOrEmpty(Convert.ToString(Session["UID"])) || string.IsNullOrEmpty(Convert.ToString(Session["TypeID"])))
        {
            Response.Redirect("~/Login.aspx");
        }

      

         //If (Session("TypeID").ToString() = "AD1") Then
         //       tr_BookingType.Visible = True
         //   End If
         //   If Session("User_Type") = "SALES" Then
         //       tr_UploadType.Visible = False
         //       tr_Cat.Visible = False
         //   End If
        if (!this.IsPostBack)
        {
            BindRole();
            BindUserID();
            BindGridview();
            Label1.InnerText = "";
        }
    }

    public void BindRole()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("BindRoleNewExecuExport_pp");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        ddlRole_type.Items.Clear();
        ddlRole_type.DataSource = cmd.ExecuteReader();
        ddlRole_type.DataTextField = "Role";
        ddlRole_type.DataValueField = "Role";
        ddlRole_type.DataBind();
        con.Close();
        ddlRole_type.Items.Insert(0, new ListItem("--Select Role--", "0"));
    }

    public void BindUserID()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("Sp_tbl_Exec");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        UserID.Items.Clear();
        UserID.DataSource = cmd.ExecuteReader();
        UserID.DataTextField = "user_id";
        UserID.DataValueField = "user_id";
        UserID.DataBind();
        con.Close();
        UserID.Items.Insert(0, new ListItem("--Select User--", "0"));
    }
    public string GetStatusVal(object val)
    {
        string value = "";
        bool result = Convert.ToBoolean(val);

        if (result == false)
        {
            value = "Inactive";
        }
        else
        {
            value = "Active";
        }
        return value;
    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {


            string result = "";

            if (ddlRole_type.SelectedValue == "0" && UserID.SelectedValue == "0")
            {
                string message = "Please Select Atleast One UserID/RoleType";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
            else
            {
                string userID="";
                string Role="";
                string RoleType = "";


                if (UserID.SelectedValue != "0" && ddlRole_type.SelectedValue != "0")
                {
                    userID = UserID.SelectedItem.Text;
                    Role = "";
                    RoleType = "";
                }
                else if (UserID.SelectedValue == "0" && ddlRole_type.SelectedValue != "0")
                {
                    userID = "";
                    Role = ddlRole_type.SelectedItem.Text;
                    RoleType = ddlRole_type.SelectedValue;
                }
                else if(UserID.SelectedValue != "0" && ddlRole_type.SelectedValue == "0")
                {
                    userID = UserID.SelectedItem.Text;
                    Role = "";
                    RoleType = "";    
                }

                bool exportpermit = false;
                bool Editpermit = false;
                bool Printpermit = false;

                //if (StatusType.SelectedValue == "True")
                //{

                //    result = insertdata(userID, RoleType, Role, true);
                //}
                //else
                //{

                //    result = insertdata(userID, RoleType, Role, false);
                //}

                if (StatusType.SelectedValue == "True")
                {
                     exportpermit = true;
                }
                if (ddleditstatus.SelectedValue == "True")
                {
                    Editpermit = true;
                }
                if (ddlprintstatus.SelectedValue == "True")
                {
                    Printpermit = true;
                }

                result = insertdata(userID, RoleType, Role, exportpermit, Editpermit, Printpermit);


                if (result == "Insert")
                {
                    BindGridview();
                    string message = "Insert Record Successfully.";
                    string script = "window.onload = function(){ alert('";
                    script += message;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);


                }
                else
                {
                    BindGridview();
                    string message = "Data Already Exists";
                    string script = "window.onload = function(){ alert('";
                    script += message;
                    script += "')};";
                    ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);

                }
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
         
        }

        ddlRole_type.SelectedIndex = 0;
        UserID.SelectedIndex = 0;
    }

    public string insertdata(string UserID,string RoleType,string Role,bool status,bool editstatus,bool printstatus)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();


        try
        {
            SqlCommand cmd = new SqlCommand("Sp_ExportPermission", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID",UserID.Trim());
            cmd.Parameters.AddWithValue("@Role", Role.Trim());
            cmd.Parameters.AddWithValue("@RoleType", RoleType.Trim());
            cmd.Parameters.AddWithValue("@PageName","");
            cmd.Parameters.AddWithValue("@pageID","");
            cmd.Parameters.AddWithValue("@Status", status);
            cmd.Parameters.AddWithValue("@editStatus", editstatus);
            cmd.Parameters.AddWithValue("@printStatus", printstatus);
            cmd.Parameters.AddWithValue("@CreatedBy", Session["UID"].ToString());


            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            return ret;

        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }


    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("SP_ExportPermission_Operations");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@flag", "Select");
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;

        GridView1.DataBind();

    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("SP_ExportPermission_Operations"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag","Delete");
                cmd.Parameters.AddWithValue("@id", Convert.ToInt32(id));

                
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string status = (row.FindControl("ddlstatus") as DropDownList).SelectedItem.ToString();
        string editstatus = (row.FindControl("ddleditstatus") as DropDownList).SelectedItem.ToString();
        string printstatus = (row.FindControl("ddlprintstatus") as DropDownList).SelectedItem.ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("SP_ExportPermission_Operations"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@flag","Update");
                cmd.Parameters.AddWithValue("@id", Convert.ToInt32(id));
                cmd.Parameters.AddWithValue("@status", status.ToUpper() == "ACTIVE" ? true : false);
                cmd.Parameters.AddWithValue("@editstatus", editstatus.ToUpper() == "ACTIVE" ? true : false);
                cmd.Parameters.AddWithValue("@printstatus", printstatus.ToUpper() == "ACTIVE" ? true : false);
                cmd.Parameters.AddWithValue("@UpdateBy", Session["UID"]);         
                cmd.Connection = con;
                con.Open();
                int i = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
                if(i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }
                
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
     
        ddlRole_type.SelectedIndex = 0;
    }
    public DataTable CheckUserDetails()
    {
        DataTable dt = new DataTable();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("CheckUserDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            con.Close();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            con.Close();
        }

        return dt;
    }

    
}