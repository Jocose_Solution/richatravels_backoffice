﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="CustomerInfo.aspx.vb" Inherits="FlightDom_CustomerInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../CSS/customerinfo.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ResolveUrl("css/itz.css") %>" rel="stylesheet" type="text/css" />
    <%--<link href="<%= ResolveUrl("css/alert.css") %>" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="../styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/Styles/jAlertCss.css")%>" rel="stylesheet" />
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3.js") %>"></script>--%>

    <style type="text/css">
        .heading {
            background-image: url(../Images/menubg.jpg);
            background-repeat: repeat-x;
            padding: 5px;
            border: thin solid #ccc;
            border-radius: 5px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function focusObj(obj) {
            if (obj.value == "First Name") obj.value = "";

        }

        function blurObj(obj) {
            if (obj.value == "") obj.value = "First Name";
        }
        function focusObj1(obj) {

            if (obj.value == "Last Name") obj.value = "";
        }

        function blurObj1(obj) {


            if (obj.value == "") obj.value = "Last Name";
        }
        function focusObjM(obj) {

            if (obj.value == "Middle Name") obj.value = "";
        }

        function blurObjM(obj) {


            if (obj.value == "") obj.value = "Middle Name";
        }


        function focusObjAir(obj) {

            if (obj.value == "Airline") obj.value = "";
        }

        function blurObjAir(obj) {


            if (obj.value == "") obj.value = "Airline";
        }
        function focusObjNumber(obj) {

            if (obj.value == "Number") obj.value = "";
        }

        function blurObjNumber(obj) {


            if (obj.value == "") obj.value = "Number";
        }

        function focusObjCFName(obj) {

            if (obj.value == "First Name") obj.value = "";
        }

        function blurObjCFName(obj) {


            if (obj.value == "") obj.value = "First Name";
        }
        function focusObjCMName(obj) {

            if (obj.value == "Middle Name") obj.value = "";
        }

        function blurObjCMName(obj) {


            if (obj.value == "") obj.value = "Middle Name";
        }
        function focusObjCLName(obj) {

            if (obj.value == "Last Name") obj.value = "";
        }

        function blurObjCLName(obj) {


            if (obj.value == "") obj.value = "Last Name";
        }





        function focusObjIFName(obj) {

            if (obj.value == "First Name") obj.value = "";
        }

        function blurObjIFName(obj) {


            if (obj.value == "") obj.value = "First Name";
        }
        function focusObjIMName(obj) {

            if (obj.value == "Middle Name") obj.value = "";
        }

        function blurObjIMName(obj) {


            if (obj.value == "") obj.value = "Middle Name";
        }
        function focusObjILName(obj) {

            if (obj.value == "Last Name") obj.value = "";
        }

        function blurObjILName(obj) {


            if (obj.value == "") obj.value = "Last Name";
        }
        function focusObjPF(obj) {

            if (obj.value == "First Name") obj.value = "";
        }
        function focusObjPL(obj) {

            if (obj.value == "Last Name") obj.value = "";
        }
        function focusObjPE(obj) {

            if (obj.value == "Email Id") obj.value = "";
        }
        function focusObjPM(obj) {

            if (obj.value == "Mobile No") obj.value = "";
        }



        function blurObjPF(obj) {

            if (obj.value == "") obj.value = "First Name";
        }
        function blurObjPL(obj) {

            if (obj.value == "") obj.value = "Last Name";
        }
        function blurObjPE(obj) {

            if (obj.value == "") obj.value = "Email Id";
        }
        function blurObjPM(obj) {

            if (obj.value == "") obj.value = "Mobile No";
        }
    </script>

    <script language="javascript" type="text/javascript">
        //ctl00_ContentPlaceHolder1_book
        $(document).ready(function () {

            var btnclk = 0;
            var vcc = document.getElementById('ctl00_ContentPlaceHolder1_hdn_vc').value;
            if (vcc == "6E" | vcc == "G8" || vcc == "SG") {

                $('#adtreq').show();
            }
            else { $('#adtreq').hide(); }

            $("#ctl00_ContentPlaceHolder1_book").click(function (e) {

                var elem = document.getElementById('ctl00_ContentPlaceHolder1_tbl_Pax').getElementsByTagName("input");
                var Adult = parseInt(0);
                var Child = parseInt(0);

                for (var i = 0; i < elem.length; i++) {
                    if (elem[i].type == "text" && elem[i].id.indexOf("txtAFirstName") > 0) {
                        Adult++;
                        if (elem[i].value == "" || elem[i].value == "First Name") {
                            //alert('First Name can not be blank for Adult');
                            //$("#dialog").dialog();
                            jAlert('First Name can not be blank for Adult', 'Alert');
                            elem[i].focus();
                            return false;
                        }
                        else if ($.trim(elem[i].value).length < 2 && elem[i].value != "First Name") {
                            //alert('First Name can not be blank for Adult');
                            //$("#dialog").dialog();
                            jAlert('First Name required atleast two characters for Adult.', 'Alert');
                            elem[i].focus();
                            return false;
                        }

                    }
                    if (elem[i].type == "text" && elem[i].id.indexOf("txtALastName") > 0) {

                        if (elem[i].value == "" || elem[i].value == "Last Name") {
                            //alert('Last Name can not be blank for Adult');
                            jAlert('Last Name can not be blank for Adult', 'Alert');
                            elem[i].focus();
                            return false;
                        }

                        else if ($.trim(elem[i].value).length < 2 && elem[i].value != "Last Name") {
                            //alert('Last Name can not be blank for Adult');
                            jAlert('Last Name required atleast two characters.', 'Alert');
                            elem[i].focus();
                            return false;
                        }
                    }
                    if (elem[i].type == "text" && (elem[i].value == "" || elem[i].value == "DOB") && elem[i].id.indexOf("Txt_AdtDOB") > 0) {
                        //alert('Age can not be blank for Adult');


                        //if (vcc == "6E" | vcc == "G8" || vcc == "SG") {
                        //    jAlert('Age can not be blank for Adult', 'Alert');
                        //    elem[i].focus();
                        //    return false;
                        //}
                    }

                    //if (document.getElementById('ctl00_ContentPlaceHolder1_hdn_vc').value == "AK") {



                    //}
                }

                //Check if Repeater Child Exsists
                if (document.getElementById('ctl00_ContentPlaceHolder1_Repeater_Child_ctl00_txtCFirstName')) {
                    for (var i = 0; i < elem.length; i++) {
                        if (elem[i].type == "text" && elem[i].id.indexOf("txtCFirstName") > 0) {
                            Child++;
                            if (elem[i].value == "" || elem[i].value == "First Name") {
                                jAlert('First Name can not be blank for Child', 'Alert');
                                elem[i].focus();
                                return false;
                            }
                            else if ($.trim(elem[i].value).length < 2 && elem[i].value != "First Name") {
                                //alert('First Name can not be blank for Adult');
                                //$("#dialog").dialog();
                                jAlert('First Name required atleast two characters for Child.', 'Alert');
                                elem[i].focus();
                                return false;
                            }

                        }
                        if (elem[i].type == "text" && elem[i].id.indexOf("txtCLastName") > 0) {
                            if (elem[i].value == "" || elem[i].value == "Last Name") {
                                jAlert('Last Name can not be blank for Child', 'Alert');
                                elem[i].focus();
                                return false;
                            }
                            else if ($.trim(elem[i].value).length < 2 && elem[i].value != "Last Name") {
                                jAlert('Last Name required atleast two characters for Child.', 'Alert');
                                elem[i].focus();
                                return false;
                            }


                        }
                        if (elem[i].type == "text" && (elem[i].value == "" || elem[i].value == "DOB") && elem[i].id.indexOf("Txt_chDOB") > 0) {
                            jAlert('Age can not be blank for Child', 'Alert');
                            elem[i].focus();
                            return false;

                        }

                    }
                }



                //Check if Repeater Infant Exsists ctl00_ContentPlaceHolder1_Repeater_Infant_ctl00_txtIFirstName
                if (document.getElementById('ctl00_ContentPlaceHolder1_Repeater_Infant_ctl00_txtIFirstName')) {

                    for (var i = 0; i < elem.length; i++) {
                        if (elem[i].type == "text" && elem[i].id.indexOf("txtIFirstName") > 0) {
                            if (elem[i].value == "" || elem[i].value == "First Name") {
                                jAlert('First Name can not be blank for Infant', 'Alert');
                                elem[i].focus();
                                return false;
                            }
                            else if ($.trim(elem[i].value).length < 2 && elem[i].value != "First Name") {
                                //alert('First Name can not be blank for Adult');
                                //$("#dialog").dialog();
                                jAlert('First Name required atleast two characters for infant.', 'Alert');
                                elem[i].focus();
                                return false;
                            }

                        }
                        if (elem[i].type == "text" && elem[i].id.indexOf("txtILastName") > 0) {
                            if (elem[i].value == "" || elem[i].value == "Last Name") {
                                jAlert('Last Name can not be blank for Infant', 'Alert');
                                elem[i].focus();
                                return false;
                            }
                            else if ($.trim(elem[i].value).length < 2 && elem[i].value != "Last Name") {
                                jAlert('Last Name required atleast two characters for infant', 'Alert');
                                elem[i].focus();
                                return false;
                            }

                        }
                        if (elem[i].type == "text" && (elem[i].value == "" || elem[i].value == "DOB") && elem[i].id.indexOf("Txt_InfantDOB") > 0) {
                            jAlert('Age can not be blank for Infant', 'Alert');
                            elem[i].focus();
                            return false;

                        }

                    }
                }


                if ($("#ctl00_ContentPlaceHolder1_DropDownListProject option:selected").val() == "Select") {
                    jAlert("Please Select Project Id", 'Alert');
                    $("#ctl00_ContentPlaceHolder1_DropDownListProject").focus();
                    return false;
                }


                if ($("#ctl00_ContentPlaceHolder1_DropDownListBookedBy option:selected").val() == "Select") {
                    jAlert("Please Select Booked By", 'Alert');
                    $("#ctl00_ContentPlaceHolder1_DropDownListBookedBy").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_PGFName").value == "" || document.getElementById("ctl00_ContentPlaceHolder1_txt_PGFName").value == "First Name") {
                    jAlert('Please Enter Primary Guest First Name', 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_PGFName").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_PGLName").value == "" || document.getElementById("ctl00_ContentPlaceHolder1_txt_PGLName").value == "Last Name") {
                    jAlert('Please Enter Primary Guest Last Name', 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_PGLName").focus();
                    return false;
                }

                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Email").value == "" || document.getElementById("ctl00_ContentPlaceHolder1_txt_Email").value == "Email Id") {
                    jAlert('Please Enter EmailID', 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_Email").focus();
                    return false;
                }

                var emailPat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                var emailid = document.getElementById("ctl00_ContentPlaceHolder1_txt_Email").value;
                var matchArray = emailid.match(emailPat);
                if (matchArray == null) {
                    jAlert("Your email address seems incorrect. Please try again.", 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_Email").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").value == "" || document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").value == "Mobile No") {
                    jAlert('Please Enter Mobile No', 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").focus();
                    return false;
                }
                else if (document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").value != "" && $.trim(document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").value).length < 10) {
                    jAlert('Please Enter atleast 10 digit in Mobile', 'Alert');
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_MobNo").focus();
                    return false;
                }


                MealBagPriceAdd(Adult, Child);


                //jConfirm('Are you sure!', 'Confirmation');
                var t;

                if (btnclk == 0) {
                    e.preventDefault();


                    jConfirm('Are you sure!', 'Confirmation', function (r) {

                        if (r) {
                            $('#ctl00_ContentPlaceHolder1_book').unbind('click').click();;
                            document.getElementById("div_Submit").style.display = "none";
                            document.getElementById("div_Progress").style.display = "block";

                            btnclk = 1;
                        }
                        else {
                            //$('#ctl00_ContentPlaceHolder1_book').bind('click');
                            btnclk == 0;
                        }

                    });



                }


                //if () {

                //    document.getElementById("div_Submit").style.display = "none";
                //    document.getElementById("div_Progress").style.display = "block";
                //    return true;
                //}
                //else {
                //    return false;
                //}
                //if (t) {
                //    alert('ok');
                //}
            });
        });
        function MealBagPriceAdd(Adult, Child) {
            debugger;
            ///// MEAL
            var A_ML_O = ""; var A_ML_I = "";
            var A_ML_O_txt = ""; A_ML_I_txt = "";
            var C_ML_O = ""; var C_ML_I = "";
            var C_ML_O_txt = ""; C_ML_I_txt = "";
            var MLAdult_f_O = parseFloat(0); MLChild_f_O = parseFloat(0);
            var MLAdult_f_I = parseFloat(0); MLChild_f_I = parseFloat(0);
            //Baggage

            var A_BG_O = ""; var A_BG_I = "";
            var A_BG_O_txt = ""; A_BG_I_txt = "";
            var C_BG_O = "", C_BG_I = "";
            var C_BG_O_txt = "", C_BG_I_txt = "";
            var BGAdult_f_O = parseFloat(0); BGChild_f_O = parseFloat(0);
            var BGAdult_f_I = parseFloat(0); BGChild_f_I = parseFloat(0);

            var Total = parseFloat(0);
            var elemddl = document.getElementById('ctl00_ContentPlaceHolder1_tbl_Pax').getElementsByTagName("select");
            //document.getElementById('ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div_ADT')
            //ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_div_ADT_Ib

            //int  Adt =0;
            for (var i = 0; i < elemddl.length; i++) {
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_A_Meal_Ob") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        A_ML_O = A_ML_O + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        A_ML_O_txt = A_ML_O_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_A_Meal_Ib") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        A_ML_I = A_ML_I + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        A_ML_I_txt = A_ML_I_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_A_EB_Ob") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        A_BG_O = A_BG_O + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        A_BG_O_txt = A_BG_O_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_A_EB_Ib") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        A_BG_I = A_BG_I + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        A_BG_I_txt = A_BG_I_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                //CHILD
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_C_Meal_Ob") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        C_ML_O = C_ML_O + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        C_ML_O_txt = C_ML_O_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_C_Meal_Ib") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        C_ML_I = C_ML_I + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        C_ML_I_txt = C_ML_I_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_C_EB_Ob") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        C_BG_O = C_BG_O + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        C_BG_O_txt = C_BG_O_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }
                if (elemddl[i].type == "select-one" && elemddl[i].id.indexOf("Ddl_C_EB_Ib") > 0) {
                    if (elemddl[i].value != "" && elemddl[i].value != null && elemddl[i].value != "select") {
                        C_BG_I = C_BG_I + elemddl[i].value + "@" + elemddl[i].options[elemddl[i].selectedIndex].text.split("INR")[1] + "}";
                        C_BG_I_txt = C_BG_I_txt + elemddl[i].options[elemddl[i].selectedIndex].text + "}";
                    }
                }

            } //for end
            var Result = "";
            //Calcualte Fares of Meal and Baggage
            //if (A_ML_O != "" || C_ML_O != "" || A_ML_I != "" || C_ML_I != "" || A_BG_O != "" || C_BG_O != "" || A_BG_I != "" || C_BG_I != "") {
            //OUTBOUND: 
            //if (A_ML_O != "" || C_ML_O != "" || A_BG_O != "" || C_BG_O != "") {
            if (A_ML_O != "") { MLAdult_f_O = CalcFare(A_ML_O_txt, MLAdult_f_O); }
            if (C_ML_O != "") { MLChild_f_O = CalcFare(C_ML_O_txt, MLChild_f_O); }
            if (A_BG_O != "") { BGAdult_f_O = CalcFare(A_BG_O_txt, BGAdult_f_O); }
            if (C_BG_O != "") { BGChild_f_O = CalcFare(C_BG_O_txt, BGChild_f_O); }


            //Insert Meal Baggage Rows inside table
            //                    if (parseFloat(MLAdult_f_O + MLChild_f_O) > 0) {
            //                        Insert_Row("OB_FT", "mp", parseInt(document.getElementById("trtotfare").rowIndex) - 1, "MealFare", parseFloat(MLAdult_f_O + MLChild_f_O));
            //                    }
            //                    if (parseFloat(BGAdult_f_O + BGChild_f_O) > 0) {
            //                        Insert_Row("OB_FT", "bp", parseInt(document.getElementById("trtotfare").rowIndex) - 1, "BagageFare", parseFloat(BGAdult_f_O + BGChild_f_O));
            //                    }

            //Total Fare Calculation
            //                    var Tot_f_O = parseFloat(0); Net_f_O = parseFloat(0);
            //                    if ($("#<%= TOT_OB_Fare.ClientID %>").val() == "") {
            //                        Tot_f_O = parseFloat(document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText);
            //                        $("#<%= TOT_OB_Fare.ClientID %>").val(Tot_f_O);
            //                    }
            //                    else {
            //                        document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= TOT_OB_Fare.ClientID %>").val()));
            //                    }
            //                    //Net Fare
            //                    if ($("#<%= NET_OB_Fare.ClientID %>").val() == "") {
            //                        Net_f_O = parseFloat(document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText);
            //                        $("#<%= NET_OB_Fare.ClientID %>").val(Net_f_O);
            //                    }
            //                    else {
            //                        document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= NET_OB_Fare.ClientID %>").val()));
            //                    }

            //document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText) + parseFloat(MLAdult_f_O + MLChild_f_O + BGAdult_f_O + BGChild_f_O));
            //document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText) + parseFloat(MLAdult_f_O + MLChild_f_O + BGAdult_f_O + BGChild_f_O));
            $("#<%= lbl_OB_TOT.ClientID %>").val(parseFloat(MLAdult_f_O + MLChild_f_O + BGAdult_f_O + BGChild_f_O));
            //}
            //INBOUND 
            //if (A_ML_I != "" || C_ML_I != "" || A_BG_I != "" || C_BG_I != "") {
            //UPdate Fare Display divFareDtlsR
            if (A_ML_I != "") { MLAdult_f_I = CalcFare(A_ML_I_txt, MLAdult_f_I); }
            if (C_ML_I != "") { MLChild_f_I = CalcFare(C_ML_I_txt, MLChild_f_I); }
            if (A_BG_I != "") { BGAdult_f_I = CalcFare(A_BG_I_txt, BGAdult_f_I); }
            if (C_BG_I != "") { BGChild_f_I = CalcFare(C_BG_I_txt, BGChild_f_I); }

            //Result= Result+ Create_MealBag_Table(MLAdult_f_I,BGAdult_f_I,MLChild_f_I,BGChild_f_I,"IB");
            Total = parseFloat(MLAdult_f_O + BGAdult_f_O + MLChild_f_O + BGChild_f_O) + parseFloat(MLAdult_f_I + BGAdult_f_I + MLChild_f_I + BGChild_f_I);
            //Result = Result+ "<div align='center'>" + Total+ "</div>"

            //Insert Meal Bagage Rows inside table - Normal Round Trip
            if (document.getElementById("trtotfareR") != null) {
                //                        if (parseFloat(MLAdult_f_I + MLChild_f_I) > 0) {
                //                            Insert_Row("IB_FT", "mpr", parseInt(document.getElementById("trtotfareR").rowIndex) - 1, "MealFare", parseFloat(MLAdult_f_I + MLChild_f_I));
                //                        }

                //                        if (parseFloat(BGAdult_f_I + BGChild_f_I) > 0) {
                //                            Insert_Row("IB_FT", "bpr", parseInt(document.getElementById("trtotfareR").rowIndex) - 1, "BagageFare", parseFloat(BGAdult_f_I + BGChild_f_I));
                //                        }

                //                        //Total Fare Calculation
                //                        var Tot_f_I = parseFloat(0); Net_f_I = parseFloat(0);
                //                        if ($("#<%= TOT_IB_Fare.ClientID %>").val() == "") {
                //                            Tot_f_I = parseFloat(document.getElementById("trtotfareR").getElementsByTagName("td")[1].innerText);
                //                            $("#<%= TOT_IB_Fare.ClientID %>").val(Tot_f_I);
                //                        }
                //                        else {
                //                            document.getElementById("trtotfareR").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= TOT_IB_Fare.ClientID %>").val()));
                //                        }
                //                        //Net Fare
                //                        if ($("#<%= NET_IB_Fare.ClientID %>").val() == "") {
                //                            Net_f_I = parseFloat(document.getElementById("trnetfareR").getElementsByTagName("td")[1].innerText);
                //                            $("#<%= NET_IB_Fare.ClientID %>").val(Net_f_I);
                //                        }
                //                        else {
                //                            document.getElementById("trnetfareR").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= NET_IB_Fare.ClientID %>").val()));
                //                        }

                //document.getElementById("trtotfareR").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trtotfareR").getElementsByTagName("td")[1].innerText) + parseFloat(MLAdult_f_I + MLChild_f_I + BGAdult_f_I + BGChild_f_I));
                //document.getElementById("trnetfareR").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trnetfareR").getElementsByTagName("td")[1].innerText) + parseFloat(MLAdult_f_I + MLChild_f_I + BGAdult_f_I + BGChild_f_I));
                $("#<%= lbl_IB_TOT.ClientID %>").val(parseFloat(MLAdult_f_I + BGAdult_f_I + MLChild_f_I + BGChild_f_I));
            }
            else //RTF Case - Relace with New Value of OUTBOUND: + Inbound 
            {
                //                        if (parseFloat(MLAdult_f_I + MLChild_f_I) > 0) {
                //                            Insert_Row("OB_FT", "mp", parseInt(document.getElementById("trtotfare").rowIndex) - 1, "MealFare", parseFloat(MLAdult_f_O + MLChild_f_O + MLAdult_f_I + MLChild_f_I));
                //                        }
                //                        if (parseFloat(BGAdult_f_I + BGChild_f_I) > 0) {
                //                            Insert_Row("OB_FT", "bp", parseInt(document.getElementById("trtotfare").rowIndex) - 1, "BagageFare", parseFloat(BGAdult_f_O + BGChild_f_O + BGAdult_f_I + BGChild_f_I));
                //                        }

                var Tot_f_O = parseFloat(0); Net_f_O = parseFloat(0);
                var Tot_f_I = parseFloat(0); Net_f_I = parseFloat(0);
                //Total Fare
                //                        if ($("#<%= TOT_OB_Fare.ClientID %>").val() == "") {
                //                            Tot_f_O = parseFloat(document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText);
                //                            $("#<%= TOT_OB_Fare.ClientID %>").val(Tot_f_O);
                //                        }
                //                        else {
                //                            document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= TOT_OB_Fare.ClientID %>").val()));
                //                        }
                //                        //Net Fare
                //                        if ($("#<%= NET_OB_Fare.ClientID %>").val() == "") {
                //                            Net_f_O = parseFloat(document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText);
                //                            $("#<%= NET_OB_Fare.ClientID %>").val(Net_f_O);
                //                        }
                //                        else {
                //                            document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat($("#<%= NET_OB_Fare.ClientID %>").val()));
                //                        }

                //document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trtotfare").getElementsByTagName("td")[1].innerText) + parseFloat(Total));
                //document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText = parseFloat(parseFloat(document.getElementById("trnetfare").getElementsByTagName("td")[1].innerText) + parseFloat(Total));
                $("#<%= lbl_OB_TOT.ClientID %>").val(parseFloat(Total));
            }

            //} //INBOUND end

            //alert("Total Fare after Meal Selection has Been Changed Please Refer Price Itineary");
            //} //IF end- Fare and Meal Calculation End

            $("#<%= lbl_A_MB_OB.ClientID %>").val(A_ML_O + "#" + A_BG_O);
            $("#<%= lbl_A_MB_IB.ClientID %>").val(A_ML_I + "#" + A_BG_I);
            $("#<%= lbl_C_MB_OB.ClientID %>").val(C_ML_O + "#" + C_BG_O);
            $("#<%= lbl_C_MB_IB.ClientID %>").val(C_ML_I + "#" + C_BG_I);
            return false;
        }
        function CalcFare(text, fare) {
            var TotFare = parseFloat(0);
            var Coll = text.split(",")
            for (var j = 0; j < Coll.length - 1; j++) {
                if (Coll[j] == "") {
                }
                else {
                    TotFare = parseFloat(TotFare) + parseFloat(Coll[j].split("INR")[1]);
                }
            }
            return TotFare;
        }

        function Insert_Row(Tablename, rowid, Pos, CellName, CellVale) {
            // Find a <table> element with id="myTable":
            if (document.getElementById(rowid) == null) {
                var table = document.getElementById(Tablename);
                // Create an empty <tr> element and add it to the 1st position of the table:
                var row = table.insertRow(Pos);
                row.id = rowid;
                // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);

                // Add some text to the new cells:
                cell1.innerHTML = CellName;
                cell2.innerHTML = CellVale;
            }
            else {//Replace Value
                document.getElementById(rowid).getElementsByTagName("td")[1].innerText = CellVale
            }
        }
    </script>

    <script type="text/javascript">
        function ddshow(id) {
            $("#ddhidebox").fadeIn(500);
            $("#closeclose").delay(1000).animate({ left: "81.7%" }, 500);
            $("#" + id + "Details").delay(500).animate({ left: "20%" }, 1000);
        }
        function ddhide() {
            $("#ddhidebox").fadeOut(500);
            $("#closeclose").animate({ left: "100%" }, 1000);
            $("#div_fareddDetails").animate({ left: "-100%" }, 1000);
            $("#ctl00_ContentPlaceHolder1_divtotFlightDetails").animate({ left: "-100%" }, 1000);
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {

                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                //alert("please enter Alphabets  only");
                return false;
            }
        }

        function showhide(layer_ref, a, b) {

            var c = layer_ref.id;
            c = c.replace(a, b);


            hza = document.getElementById(c);
            if (hza.style.display == 'none') {
                hza.style.display = 'block';
            }
            else {
                hza.style.display = 'none';
            }
        }
    </script>
    <div style="margin-top: 30px;"></div>
    <div class="large-10 medium-10 small-12 large-push-1 medium-push-1">

        <div class="large-12 medium-12 small-12">
            <div id="divFltDtls1" runat="server" class="large-9 medium-9 small-12 columns" style="padding-top: 10px;">
            </div>
            <div id="divtotalpax" runat="server" class="large-3 medium-3 small-12 columns" style="padding-top: 10px;">
            </div>

        </div>

        <div class="clear1 w100">
            <hr />
        </div>

        <div class="large-12 medium-12 small-12">
            <div id="tbl_Pax" runat="server" class="w100">
                <div id="td_Adult" runat="server">
                    <asp:Repeater ID="Repeater_Adult" runat="server" OnItemCreated="Repeater_Adult_ItemCreated">
                        <ItemTemplate>
                            <div class="row">
                                <div class="large-10 medium-10 small-12 columns bld">
                                    <asp:Label ID="pttextADT" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaxTP")%>'></asp:Label>
                                </div>

                                <div class="clear1"></div>
                                <div class="large-12 medium-12 small-12">
                                    <div class="large-1 medium-1 small-3 columns lft">
                                        <asp:DropDownList ID="ddl_ATitle" runat="server">
                                            <%--  <asp:ListItem Value="" Selected="True">Title</asp:ListItem>--%>
                                            <asp:ListItem Value="Mr">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Miss">Miss.</asp:ListItem>
                                            <asp:ListItem Value="Ms">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
                                            <%--<asp:ListItem Value="Miss">Dr.</asp:ListItem>
                                        <asp:ListItem Value="Miss">Prof.</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="large-1 medium-1 small-3 columns lft" style="display: none;">
                                        <asp:DropDownList CssClass="txtdd1" ID="ddl_AGender" runat="server">
                                            <%--  <asp:ListItem Value="" Selected="True">Title</asp:ListItem>--%>
                                            <asp:ListItem Value="M">Male</asp:ListItem>
                                            <asp:ListItem Value="F">Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="large-2 medium-2 small-8 columns">
                                        <asp:TextBox ID="txtAFirstName" runat="server" value="First Name"
                                            onfocus="focusObj(this);" onblur="blurObj(this);" defvalue="First Name" autocomplete="off"
                                            onkeypress="return isCharKey(event)"></asp:TextBox>
                                    </div>
                                    <div class="large-1 medium-1 small-1 columns red">*</div>
                                    <div class="large-2 medium-2 small-12 passenger columns">
                                        <asp:TextBox ID="txtAMiddleName" runat="server" value="Middle Name"
                                            onfocus="focusObjM(this);" onblur="blurObjM(this);" defvalue="Middle Name" onkeypress="return isCharKey(event)"></asp:TextBox>
                                    </div>

                                    <div class="large-2 medium-2 small-11 columns large-push-1 medium-push-1">
                                        <asp:TextBox ID="txtALastName" runat="server" value="Last Name"
                                            onfocus="focusObj1(this);" onblur="blurObj1(this);" defvalue="Last Name" onkeypress="return isCharKey(event)"></asp:TextBox>
                                    </div>
                                    <div class="large-1 medium-1 small-1 columns red large-push-1 medium-push-1">*</div>
                                    <div class="large-1 medium-1 small-11 columns lft large-push-1 medium-push-1">
                                        <asp:TextBox CssClass="adtdobcss" value="DOB" ID="Txt_AdtDOB"  runat="server"></asp:TextBox>
                                    </div>
                                    <div id="adtreq" class="large-1 medium-1 small-1 columns red"></div>
                                </div>

                            </div>


                            <div class="row" id="A_SG">
                                <div class="large-12 medium-12 small-12 columns" id="div_ADT" runat="server" style="display: none;">

                                    <div class="large-12 medium-12 small-12 columns bld blue">
                                        OutBound:<span style="color: #004b91">---</span>
                                    </div>

                                    <div class="large-12 medium-12 small-12 columns">
                                        <div class="large-1 medium-1 small-3 columns">
                                            Meal
                                        </div>
                                        <div class="large-3 medium-3 small-3 columns">
                                            <asp:DropDownList ID="Ddl_A_Meal_Ob" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="large-2 medium-2 small-3 columns large-push-3 medium-push-3">
                                            Excess Baggage
                                        </div>
                                        <div class="large-3 medium-3 small-3 columns">
                                            <asp:DropDownList CssClass="txtdd1" ID="Ddl_A_EB_Ob" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div runat="server" id="tranchor1">
                                    <div class="clear">
                                    </div>


                                    <div class="large-12 medium-12 small-12 columns lft blue bld">
                                        OutBound:<span style="color: #004b91">---</span>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="large-12 medium-12 small-12 columns lft">
                                        <a runat="server" id='anchor1' class="cursorpointer" onclick="showhide(this,'anchor1','div1');">Meal Preference/Seat Preference/Frequent Flyer</a>
                                    </div>
                                </div>
                                <div id="A_ALL" runat="server">
                                    <div id="div1" runat="server" style="display: none;">
                                        <div class="large-12 medium-12 small-12 columns">
                                            <div class="large-1 medium-1 small-2 columns">
                                                Meal
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                <asp:DropDownList CssClass="txtdd1" ID="ddl_AMealPrefer" runat="server">
                                                    <asp:ListItem Value="">No Pref.</asp:ListItem>
                                                    <asp:ListItem Value="VGML">Vegetarian</asp:ListItem>
                                                    <asp:ListItem Value="AVML">Asian</asp:ListItem>
                                                    <asp:ListItem Value="SFML">Seafood</asp:ListItem>
                                                    <asp:ListItem Value="KSML">Kosher</asp:ListItem>
                                                    <asp:ListItem Value="MOML">Muslim</asp:ListItem>
                                                    <asp:ListItem Value="HNML">Hindu</asp:ListItem>
                                                    <asp:ListItem Value="LFML">Low Fat</asp:ListItem>
                                                    <asp:ListItem Value="LCML">Low Calorie</asp:ListItem>
                                                    <asp:ListItem Value="LPML">Low Protein</asp:ListItem>
                                                    <asp:ListItem Value="GFML">Gluten Free</asp:ListItem>
                                                    <asp:ListItem Value="HFML">High Fiber</asp:ListItem>
                                                    <asp:ListItem Value="DBML">Diabetic</asp:ListItem>
                                                    <asp:ListItem Value="NLML">Non-lactose</asp:ListItem>
                                                    <asp:ListItem Value="PRML">Low Purin</asp:ListItem>
                                                    <asp:ListItem Value="RVML">Raw Vegetarian</asp:ListItem>
                                                    <asp:ListItem Value="CHML">Child</asp:ListItem>
                                                    <asp:ListItem Value="BLML">Bland</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="large-1 medium-1 small-2 columns">
                                                Seat
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                <asp:DropDownList CssClass="txtdd1" ID="ddl_ASeatPrefer" runat="server">
                                                    <asp:ListItem Value="">Any</asp:ListItem>
                                                    <asp:ListItem Value="W">Window</asp:ListItem>
                                                    <asp:ListItem Value="A">Aisle</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="large-2 medium-2 small-4 columns">
                                                Frequent Flyer 
                                            </div>
                                           <div class="large-2 medium-2 small-4 columns">
                                                <asp:TextBox ID="txt_AAirline" runat="server" value="Airline"
                                                  onfocus="focusObjAir(this);" onblur="blurObjAir(this);" defvalue="Airline" CssClass="Airlineval"></asp:TextBox>
                                                <input type="hidden" id="hidtxtAirline_Dom" name="hidtxtAirline_dom" runat="server"  value="" />

                                            </div>
                                            <div class="large-2 medium-2 small-4 columns lft">
                                                <asp:TextBox ID="txt_ANumber" runat="server" MaxLength="10" value="Number"
                                                    onfocus="focusObjNumber(this);" onblur="blurObjNumber(this);" defvalue="Number"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="clear1"></div>
                            <div class="row" id="A_SG_IB">
                                <div id="div_ADT_Ib" class="large-12 medium-12 small-12 columns" runat="server" style="display: none">

                                    <div class="large-12 medium-12 small-12 columns blue bld">
                                        InBound:<span style="color: #004b91">---</span>
                                    </div>
                                    <div class="large-12 medium-12 small-12 columns">
                                        <div class="large-1 medium-1 small-3 columns">
                                            Meal
                                        </div>
                                        <div class="large-3 medium-3 small-3 columns">
                                            <asp:DropDownList ID="Ddl_A_Meal_Ib" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="large-2 medium-2 small-3 columns large-push-3 medium-push-3">
                                            Excess Baggage
                                        </div>
                                        <div class="large-3 medium-3 small-3 columns">
                                            <asp:DropDownList CssClass="txtdd1" ID="Ddl_A_EB_Ib" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <%-- <div class="clear">
                                        </div>--%>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div runat="server" id="tranchor1_R" style="display: none">

                                    <div class="large-12 medium-12 small-12 columns bld blue">
                                        InBound:<span style="color: #004b91">---</span>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="large-12 medium-12 small-12 columns">
                                        <a runat="server" id='anchor1_R' onclick="showhide(this,'anchor1_R','div1_R');" class="cursorpointer">Meal Preference/Seat Preference/Frequent Flyer</a>
                                    </div>
                                </div>
                                <div id="A_ALL_R" runat="server" style="display: none;">
                                    <div id="div1_R" runat="server" class="w100" style="display: none;">
                                        <div class="large-12 medium-12 small-12 columns">
                                            <div class="large-1 medium-1 small-2 columns">
                                                Meal
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                <asp:DropDownList CssClass="txtdd1" ID="ddl_AMealPrefer_R" runat="server" Width="100px">
                                                    <asp:ListItem Value="">No Pref.</asp:ListItem>
                                                    <asp:ListItem Value="VGML">Vegetarian</asp:ListItem>
                                                    <asp:ListItem Value="AVML">Asian</asp:ListItem>
                                                    <asp:ListItem Value="SFML">Seafood</asp:ListItem>
                                                    <asp:ListItem Value="KSML">Kosher</asp:ListItem>
                                                    <asp:ListItem Value="MOML">Muslim</asp:ListItem>
                                                    <asp:ListItem Value="HNML">Hindu</asp:ListItem>
                                                    <asp:ListItem Value="LFML">Low Fat</asp:ListItem>
                                                    <asp:ListItem Value="LCML">Low Calorie</asp:ListItem>
                                                    <asp:ListItem Value="LPML">Low Protein</asp:ListItem>
                                                    <asp:ListItem Value="GFML">Gluten Free</asp:ListItem>
                                                    <asp:ListItem Value="HFML">High Fiber</asp:ListItem>
                                                    <asp:ListItem Value="DBML">Diabetic</asp:ListItem>
                                                    <asp:ListItem Value="NLML">Non-lactose</asp:ListItem>
                                                    <asp:ListItem Value="PRML">Low Purin</asp:ListItem>
                                                    <asp:ListItem Value="RVML">Raw Vegetarian</asp:ListItem>
                                                    <asp:ListItem Value="CHML">Child</asp:ListItem>
                                                    <asp:ListItem Value="BLML">Bland</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="large-1 medium-1 small-2 columns">
                                                Seat
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                <asp:DropDownList CssClass="txtdd1" ID="ddl_ASeatPrefer_R" runat="server">
                                                    <asp:ListItem Value="">Any</asp:ListItem>
                                                    <asp:ListItem Value="W">Window</asp:ListItem>
                                                    <asp:ListItem Value="A">Aisle</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                Frequent Flyer 
                                            </div>
                                             
                                           <div class="large-2 medium-2 small-4 columns">
                                                <asp:TextBox ID="txt_AAirline_R" runat="server" value="Airline"
                                                 onfocus="focusObjAir(this);" onblur="blurObjAir(this);" defvalue="Airline" CssClass="Airlineval_R"></asp:TextBox>
                                                <input type="hidden" id="hidtxtAirline_R_dom" name="hidtxtAirline_R_Dom" runat="server" value="" />
                                            </div>
                                            <div class="large-2 medium-2 small-4 columns">
                                                <asp:TextBox ID="txt_ANumber_R" runat="server" MaxLength="10"
                                                    value="Number" 
                                                    defvalue="Number"></asp:TextBox>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>

                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div id="td_Child" runat="server">
                    <asp:Repeater ID="Repeater_Child" runat="server" OnItemCreated="Repeater_Child_ItemCreated">
                        <ItemTemplate>
                            <div style="margin-bottom: 5px;">
                                <div class="txt1 bld">
                                    <asp:Label ID="pttextCHD" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaxTP")%>'></asp:Label>
                                </div>
                                <div class="clear1"></div>
                                <div class="row">
                                    <div class="large-12 medium-12 small-12">
                                        <div class="large-1 medium-1 small-3 columns">
                                            <asp:DropDownList ID="ddl_CTitle" runat="server">
                                                <asp:ListItem Value="Mstr">Mstr.</asp:ListItem>
                                                <asp:ListItem Value="Miss">Miss.</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="large-2 medium-2 small-8 columns">
                                            <asp:TextBox ID="txtCFirstName" runat="server" value="First Name"
                                                onfocus="focusObjCFName(this);" onblur="blurObjCFName(this);" defvalue="First Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red">*</div>
                                        <div class="large-2 medium-2 small-12 passenger columns lft">
                                            <asp:TextBox ID="txtCMiddleName" runat="server" value="Middle Name"
                                                onfocus="focusObjCMName(this);" onblur="blurObjCMName(this);" defvalue="Middle Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-2 medium-2 small-11 columns large-push-1 medium-push-1">
                                            <asp:TextBox ID="txtCLastName" runat="server" value="Last Name"
                                                onfocus="focusObjCLName(this);" onblur="blurObjCLName(this);" defvalue="Last Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red large-push-1 medium-push-1">*</div>
                                        <div class="large-1 medium-1 small-11 columns lft large-push-1 medium-push-1">
                                            <asp:TextBox CssClass="txtboxx chddobcss" ID="Txt_chDOB" runat="server" value="DOB"> </asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red">*</div>
                                    </div>
                                </div>
                                <div class="w100">
                                    <div id="C_SG">

                                        <div class="large-12 medium-12 small-12 columns" id="div_CHILD" runat="server" style="display: none;">

                                            <div class="large-12 medium-12 small-12 columns bld blue">
                                                OutBound:<span style="color: #004b91">---</span>
                                            </div>

                                            <div class="large-12 medium-12 small-12 columns">
                                                <div class="large-1 medium-1 small-3 columns">
                                                    Meal
                                                </div>
                                                <div class="large-3 medium-3 small-3 columns">
                                                    <asp:DropDownList ID="Ddl_C_Meal_Ob" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns large-push-3 medium-push-3">
                                                    Excess Baggage
                                                </div>
                                                <div class="large-3 medium-3 small-3 columns">
                                                    <asp:DropDownList CssClass="txtdd1" ID="Ddl_C_EB_Ob" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>


                                        <div runat="server" id="tranchor2">
                                            <div class="clear">
                                            </div>
                                            <div class="txt2">
                                                OutBound:<span style="color: #004b91">---</span>
                                            </div>
                                            <div class="hide">
                                            </div>
                                            <div class="w101" style="padding-top: 10px;">
                                                <a runat="server" id='anchor2' onclick="showhide(this,'anchor2','div2');" class="cursorpointer">Meal Preference/Seat Preference</a>
                                            </div>
                                        </div>
                                        <div id="C_ALL" runat="server">
                                            <div id="div2" runat="server" class="large-12 medium-12 small-12" style="display: none;">
                                                <div class="large-1 medium-1 small-2 columns">
                                                    Meal
                                                </div>
                                                <div class="large-2 medium-2 small-4 columns">
                                                    <asp:DropDownList CssClass="txtdd1" ID="ddl_CMealPrefer" runat="server" Width="100px">
                                                        <asp:ListItem Value="">No Pref.</asp:ListItem>
                                                        <asp:ListItem Value="VGML">Vegetarian</asp:ListItem>
                                                        <asp:ListItem Value="AVML">Asian</asp:ListItem>
                                                        <asp:ListItem Value="SFML">Seafood</asp:ListItem>
                                                        <asp:ListItem Value="KSML">Kosher</asp:ListItem>
                                                        <asp:ListItem Value="MOML">Muslim</asp:ListItem>
                                                        <asp:ListItem Value="HNML">Hindu</asp:ListItem>
                                                        <asp:ListItem Value="LFML">Low Fat</asp:ListItem>
                                                        <asp:ListItem Value="LCML">Low Calorie</asp:ListItem>
                                                        <asp:ListItem Value="LPML">Low Protein</asp:ListItem>
                                                        <asp:ListItem Value="GFML">Gluten Free</asp:ListItem>
                                                        <asp:ListItem Value="HFML">High Fiber</asp:ListItem>
                                                        <asp:ListItem Value="DBML">Diabetic</asp:ListItem>
                                                        <asp:ListItem Value="NLML">Non-lactose</asp:ListItem>
                                                        <asp:ListItem Value="PRML">Low Purin</asp:ListItem>
                                                        <asp:ListItem Value="RVML">Raw Vegetarian</asp:ListItem>
                                                        <asp:ListItem Value="CHML">Child</asp:ListItem>
                                                        <asp:ListItem Value="BLML">Bland</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="large-1 medium-1 small-2 columns">
                                                    Seat
                                                </div>
                                                <div class="large-2 medium-2 small-4 columns">
                                                    <asp:DropDownList CssClass="txtdd1" ID="ddl_CSeatPrefer" runat="server">
                                                        <asp:ListItem Value="">Any</asp:ListItem>
                                                        <asp:ListItem Value="W">Window</asp:ListItem>
                                                        <asp:ListItem Value="A">Aisle</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="C_SG_IB">
                                        <div id="div_CHILD_Ib" class="large-12 medium-12 small-12 columns" runat="server" style="display: none">

                                            <div class="large-12 medium-12 small-12 columns blue bld">
                                                InBound:<span style="color: #004b91">---</span>
                                            </div>
                                            <div class="large-12 medium-12 small-12 columns">
                                                <div class="large-1 medium-1 small-3 columns">
                                                    Meal
                                                </div>
                                                <div class="large-3 medium-3 small-3 columns">
                                                    <asp:DropDownList ID="Ddl_C_Meal_Ib" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns large-push-3 medium-push-3">
                                                    Excess Baggage
                                                </div>
                                                <div class="large-3 medium-3 small-3 columns">
                                                    <asp:DropDownList CssClass="txtdd1" ID="Ddl_C_EB_Ib" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <%-- <div class="clear">
                                        </div>--%>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>



                                        <div id="tranchor2_R" runat="server" class="hide">
                                            <div class="clear">
                                            </div>
                                            <div class="txt2 blue bld">
                                                InBound:<span style="color: #004b91">---</span>
                                            </div>
                                            <div style="clear: both">
                                            </div>
                                            <div class="w101" style="padding-top: 10px;">
                                                <a runat="server" id='anchor2_R' onclick="showhide(this,'anchor2_R','div2_R');" class="cursorpointer">Meal Preference/Seat Preference</a>
                                            </div>
                                        </div>
                                        <div id="C_ALL_R" runat="server" style="display: none;">
                                            <div id="div2_R" runat="server" style="display: none;">
                                                <div class="w101" style="padding-top: 10px;">
                                                    <div class="w10 lft">
                                                        Meal
                                                    </div>
                                                    <div class="w15 lft">
                                                        <asp:DropDownList CssClass="txtdd1" ID="ddl_CMealPrefer_R" runat="server" Width="100px">
                                                            <asp:ListItem Value="">No Pref.</asp:ListItem>
                                                            <asp:ListItem Value="VGML">Vegetarian</asp:ListItem>
                                                            <asp:ListItem Value="AVML">Asian</asp:ListItem>
                                                            <asp:ListItem Value="SFML">Seafood</asp:ListItem>
                                                            <asp:ListItem Value="KSML">Kosher</asp:ListItem>
                                                            <asp:ListItem Value="MOML">Muslim</asp:ListItem>
                                                            <asp:ListItem Value="HNML">Hindu</asp:ListItem>
                                                            <asp:ListItem Value="LFML">Low Fat</asp:ListItem>
                                                            <asp:ListItem Value="LCML">Low Calorie</asp:ListItem>
                                                            <asp:ListItem Value="LPML">Low Protein</asp:ListItem>
                                                            <asp:ListItem Value="GFML">Gluten Free</asp:ListItem>
                                                            <asp:ListItem Value="HFML">High Fiber</asp:ListItem>
                                                            <asp:ListItem Value="DBML">Diabetic</asp:ListItem>
                                                            <asp:ListItem Value="NLML">Non-lactose</asp:ListItem>
                                                            <asp:ListItem Value="PRML">Low Purin</asp:ListItem>
                                                            <asp:ListItem Value="RVML">Raw Vegetarian</asp:ListItem>
                                                            <asp:ListItem Value="CHML">Child</asp:ListItem>
                                                            <asp:ListItem Value="BLML">Bland</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="w5 lft">
                                                        Seat
                                                    </div>
                                                    <div class="w10 lft">
                                                        <asp:DropDownList CssClass="txtdd1" ID="ddl_CSeatPrefer_R" runat="server">
                                                            <asp:ListItem Value="">Any</asp:ListItem>
                                                            <asp:ListItem Value="W">Window</asp:ListItem>
                                                            <asp:ListItem Value="A">Aisle</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear1">
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>



                <div id="td_Infant" runat="server">
                    <asp:Repeater ID="Repeater_Infant" runat="server">
                        <ItemTemplate>
                            <div style="margin-bottom: 5px;">
                                <div class="txt1 bld">
                                    <asp:Label ID="pttextINF" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaxTP")%>'></asp:Label>
                                </div>
                                <div class="clear1"></div>
                                <div class="row">
                                    <div class="large-12 medium-12 small-12">

                                        <div class="large-1 medium-1 small-3 columns">
                                            <asp:DropDownList CssClass="txtdd1" ID="ddl_ITitle" runat="server">
                                                <%-- <asp:ListItem Value="">Title</asp:ListItem>--%>
                                                <asp:ListItem Value="Mstr">Mstr.</asp:ListItem>
                                                <asp:ListItem Value="Miss">Miss.</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="large-2 medium-2 small-8 columns">
                                            <asp:TextBox ID="txtIFirstName" runat="server" value="First Name"
                                                onfocus="focusObjIFName(this);" onblur="blurObjIFName(this);" defvalue="First Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red">*</div>
                                        <div class="large-2 medium-2 small-12 passenger columns">
                                            <asp:TextBox ID="txtIMiddleName" runat="server" value="Middle Name"
                                                onfocus="focusObjIMName(this);" onblur="blurObjIMName(this);" defvalue="Middle Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-2 medium-2 small-11 columns large-push-1 medium-push-1">
                                            <asp:TextBox ID="txtILastName" runat="server" value="Last Name"
                                                onfocus="focusObjILName(this);" onblur="blurObjILName(this);" defvalue="Last Name"
                                                onkeypress="return isCharKey(event)"></asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red large-push-1 medium-push-1">*</div>

                                        <div class="large-1 medium-1 small-11 columns lft large-push-1 medium-push-1">

                                            <asp:TextBox CssClass="txtboxx infdobcss" value="DOB" ID="Txt_InfantDOB"  runat="server"></asp:TextBox>
                                        </div>
                                        <div class="large-1 medium-1 small-1 columns red">
                                            *
                                        </div>

                                    </div>
                                </div>
                                <div class="clear1">
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="clear"></div>
            <div class="large-12 medium-12 small-12 bld">
                Primary Guest Details
            </div>

            <div class="row">

                <div class="clear1"></div>

                <div class="large-12 medium-12 small-12">
                    <div class="large-3 medium-3 small-6 columns lft">
                        <span id="spn_Projects" runat="server">Project Id</span>
                    </div>
                    <div class="large-3 medium-3 small-6 columns lft">
                        <span id="spn_Projects1" runat="server">
                            <asp:DropDownList CssClass="txtdd1" ID="DropDownListProject" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="large-3 medium-3 small-6 columns lft">
                        <span id="Spn_BookedBy" runat="server">Booked By </span>
                    </div>
                    <div class="large-3 medium-3 small-6 columns lft">
                        <span id="Spn_BookedBy1" runat="server">
                            <asp:DropDownList CssClass="txtdd1" ID="DropDownListBookedBy" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="large-12 medium-12 small-12">
                    <div class="large-1 medium-1 small-3 columns">
                        <asp:DropDownList CssClass="txtdd1" ID="ddl_PGTitle" runat="server">
                            <%--<asp:ListItem Value="" Selected="True">Select Title</asp:ListItem>--%>
                            <asp:ListItem Value="Mr">Mr.</asp:ListItem>
                            <asp:ListItem Value="Miss">Miss.</asp:ListItem>
                            <asp:ListItem Value="Ms">Ms.</asp:ListItem>
                            <asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
                            <%--<asp:ListItem Value="Miss">Dr.</asp:ListItem>
                            <asp:ListItem Value="Miss">Prof.</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="large-2 medium-2 small-8 columns">
                        <asp:TextBox ID="txt_PGFName" runat="server" value="First Name"
                            onfocus="focusObjPF(this);" onblur="blurObjPF(this);" defvalue="First Name" onkeypress="return isCharKey(event)"></asp:TextBox>
                    </div>
                    <div class="large-1 medium-1 small-1 columns red">*</div>
                    <div class="large-2 medium-2 small-11 columns">
                        <asp:TextBox ID="txt_PGLName" runat="server" value="Last Name"
                            onfocus="focusObjPL(this);" onblur="blurObjPL(this);" defvalue="Last Name" onkeypress="return isCharKey(event)"></asp:TextBox>
                    </div>
                    <div class="large-1 medium-1 small-1 columns red">*</div>
                    <div class="large-2 medium-2 small-11 columns">
                        <asp:TextBox ID="txt_Email" value="Email Id" onfocus="focusObjPE(this);"
                            onblur="blurObjPE(this);" defvalue="Email Id" runat="server"></asp:TextBox>
                    </div>
                    <div class="large-1 medium-1 small-1 columns red">*</div>
                    <div class="large-1 medium-1 small-11 columns">
                        <asp:TextBox ID="txt_MobNo" runat="server" value="Mobile No" onfocus="focusObjPM(this);"
                            onblur="blurObjPM(this);" defvalue="Mobile No" onkeypress="return isNumberKey(event)"
                            MaxLength="12" oncopy="return false" onpaste="return false"></asp:TextBox>
                    </div>
                    <div class="large-1 medium-1 small-1 columns red">*</div>
                </div>




            </div>



            <div class="clear1"></div>

            <div id="div_Progress" style="display: none">
                <b>Booking In Progress.</b> Please do not 'refresh' or 'back' button
                <img alt="Booking In Progress" src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
            </div>

            <div class="large-12 medium-12 small-12">

                <div id="div_Submit" class="large-2 medium-2 small-12 large-push-10 medium-push-10 columns">
                    <asp:Button ID="book" runat="server" Text="Continue" />
                </div>
            </div>

        </div>

        <div class="clear">
        </div>
        <div id="ddhidebox" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: url(../images/fade.png); display: none;">
            <div id="divtotFlightDetails" runat="server" style="width: 58%; left: -100%; position: fixed; padding: 1.5%; box-shadow: 1px 2px 10px #000; border: 5px solid #d1d1d1; margin-top: 100px; background: #f1f1f1; float: left;">
            </div>
            <div id="div_fareddDetails" style="width: 58%; border: 5px solid #d1d1d1; left: -100%; position: fixed; padding: 1.5%; box-shadow: 1px 2px 10px #000; margin-top: 100px; background: #f1f1f1; float: left;">
                <div id="div_fare" runat="server" class="lft w100">
                </div>
                <div id="div_fareR" runat="server" class="lft w100">
                </div>
            </div>
            <div style="padding: 5px; background: #f1f1f1; margin-top: 100px; font-weight: bold; position: fixed; left: 100%; cursor: pointer;"
                onclick="ddhide();" id="closeclose">
                CLOSE
            </div>
        </div>
        <div class="">
            <asp:Label ID="lbl_adult" runat="server" CssClass="hide"></asp:Label>
            <asp:Label ID="lbl_child" runat="server" CssClass="hide"></asp:Label>
            <asp:Label ID="lbl_infant" runat="server" CssClass="hide"></asp:Label>
            <asp:HiddenField ID="lbl_A_MB_OB" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="lbl_A_MB_IB" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="lbl_C_MB_OB" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="lbl_C_MB_IB" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="lbl_OB_TOT" EnableViewState="true" runat="server" Value="0" />
            <asp:HiddenField ID="lbl_IB_TOT" EnableViewState="true" runat="server" Value="0" />
            <asp:HiddenField ID="TOT_OB_Fare" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="NET_OB_Fare" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="TOT_IB_Fare" EnableViewState="true" runat="server" />
            <asp:HiddenField ID="NET_IB_Fare" EnableViewState="true" runat="server" />
        </div>



        <div class="clear1">
        </div>


        <%--<div id="dialog" title="Alert" style="border:2px solid #ddd !important; font-size:12px !important;">
  <p style="font-size:12px !important;">This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>--%>

        <asp:HiddenField ID="hdn_vc" runat="server" />

        <script type="text/javascript" src="../js/chrome.js"></script>

        <script type="text/javascript">
            function funcnetfare(arg, id) {
                document.getElementById(id).style.display = arg

            }
        </script>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.draggable.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
        <script type="text/javascript">
            var d = new Date();

            $(function () { var d = new Date(); var dd = new Date(1952, 01, 01); $(".adtdobcss").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, yearRange: ('1952:' + (d.getFullYear() - 12) + ''), navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true, defaultDate: dd }) });
            $(function () { $(".chddobcss").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '-2y', minDate: '-12y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
            $(function () { $(".infdobcss").datepicker({ numberOfMonths: 1, dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true, maxDate: '+0y', minDate: '-2y', navigationAsDateFormat: true, showOtherMonths: true, selectOtherMonths: true }) });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {

                $("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_ATitle").change(function () {
                    $("#ctl00_ContentPlaceHolder1_ddl_PGTitle").val($("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_ddl_ATitle").val());
                });

                $("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAFirstName").live("keyup", function () {
                    $("#ctl00_ContentPlaceHolder1_txt_PGFName").val($("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAFirstName").val())
                });

                $("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAFirstName").live("blur", function () {
                    $("#ctl00_ContentPlaceHolder1_txt_PGFName").val($("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtAFirstName").val())
                });

                $("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtALastName").live("keyup", function () {
                    $("#ctl00_ContentPlaceHolder1_txt_PGLName").val($("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtALastName").val())
                });

                $("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtALastName").live("blur", function () {
                   $("#ctl00_ContentPlaceHolder1_txt_PGLName").val($("#ctl00_ContentPlaceHolder1_Repeater_Adult_ctl00_txtALastName").val())
                });

                var aircode = $('.Airlineval').each(function () {
                    $(this).autocomplete({
                        source: function (e, t) {
                            $.ajax({
                                url: UrlBase + "CitySearch.asmx/FetchAirlineList",
                                data: "{ 'airline': '" + e.term + "', maxResults: 10 }",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (e) {
                                    t($.map(e.d, function (e) {
                                        var t = e.ALName + "(" + e.ALCode + ")";
                                        var n = e.ALCode;
                                        return {
                                            label: t,
                                            value: t,
                                            id: n
                                        }
                                    }))
                                },
                                error: function (e, t, n) {
                                    alert(t)
                                }
                            })
                        },
                        autoFocus: true,
                        minLength: 3,
                        select: function (t, n) {
                            $(this).next().val(n.item.id)
                        }
                    });
                });
                
                var aircode = $('.Airlineval_R').each(function () {
                    $(this).autocomplete({
                        source: function (e, t) {
                            $.ajax({
                                url: UrlBase + "CitySearch.asmx/FetchAirlineList",
                                data: "{ 'airline': '" + e.term + "', maxResults: 10 }",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (e) {
                                    t($.map(e.d, function (e) {
                                        var t = e.ALName + "(" + e.ALCode + ")";
                                        var n = e.ALCode;
                                        return {
                                            label: t,
                                            value: t,
                                            id: n
                                        }
                                    }))
                                },
                                error: function (e, t, n) {
                                    alert(t)
                                }
                            })
                        },
                        autoFocus: true,
                        minLength: 3,
                        select: function (t, n) {
                            $(this).next().val(n.item.id)
                        }
                    });
                });
            });
        </script>
</asp:Content>
