﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class OtpValidate
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction
    Dim AgentType As String
    Dim series As New SeriesDepart
    Private objSql As New SqlTransactionNew
    Dim objSMSAPI As New SMSAPI.SMS
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then


                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                If (Session("UID") = "" Or Session("UID") Is Nothing) Then
                    Response.Redirect("~/Login.aspx")
                End If
                Dim Dt As New DataTable
                Dim saAllowedCharacters As String() = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"}
                Dim sRandomOTP As String = GenerateRandomOTP(6, saAllowedCharacters)

                Dim agent As String = Request("AgentID")
                Dim amount As String = Request("Amount")
                Dim Counter As String = Request("ID")
                Dt = ST.GetAgencyDetails(agent).Tables(0)
                Dim Avalbalance As String = Dt.Rows(0)("Crd_Limit").ToString
                Dim Agencyname As String = Dt.Rows(0)("Agency_Name").ToString
                Dim Mobile As String = Dt.Rows(0)("Mobile").ToString
                Dim Agent_Type As String = Dt.Rows(0)("Agent_Type").ToString
                Dim DueAmount As String = Convert.ToString(Dt.Rows(0)("DueAmount"))
                Dim AgentLimit As String = Convert.ToString(Dt.Rows(0)("AgentLimit"))
                Agencyname = Agencyname + "(" + agent + ")"
                Dim transid As String = Guid.NewGuid.ToString().Replace("-", "")
                Dim Ab As Integer = InsertOtp(Session("UID").ToString(), transid, sRandomOTP, "9825326799", "CreditMoney")
                If Ab = 1 Then
                    SendSMS(Session("UID").ToString(), Agencyname, Avalbalance, Mobile, Agent_Type, DueAmount, AgentLimit, amount, sRandomOTP)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btn_sumit_Click(sender As Object, e As EventArgs)
        Try
            Dim otp As String = txtotp.Text
            Dim otpdt As DataTable
            otpdt = ST.Getotpdetails(otp, "9825326799", Session("UID").ToString()).Tables(0)
            If (otp = Convert.ToString(otpdt.Rows(0)("UpdateOtp"))) Then
                Response.Redirect("SprReports/Accounts/UploadCredit.aspx?AgentID=" + Request("AgentID") + "&ID=" + Request("ID") + "&Amount=" + Request("Amount"))

            ElseIf (Convert.ToString(otpdt.Rows(0)("UpdateOtp")) = "100") Then
                lvlval.Text = "OTP is valid for 5 mins"

            ElseIf (Convert.ToString(otpdt.Rows(0)("UpdateOtp")) = "101") Then
                lvlval.Text = "you have entered a wrong otp"

            End If





        Catch ex As Exception

        End Try

    End Sub

    Private Sub SendSMS(ByVal Execid As String, ByVal Agencyname As String, ByVal Avalbalance As String, ByVal Mobile As String, ByVal Agent_Type As String, ByVal DueAmount As String, ByVal AgentLimit As String, ByVal amount As String, ByVal sRandomOTP As String)
        Try
            Dim smsStatus As String = ""
            Dim smsMsg As String = ""
            Try
                Dim SmsCrd As DataTable
                SmsCrd = ST.SmsCredential("AGENTREGISTER").Tables(0)
                'SmsCrd = ST.SmsCredential(SMS.AIRBOOKINGDOM.ToString()).Tables(0)
                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                    smsStatus = objSMSAPI.TransactionOTP(Execid, Agencyname, Avalbalance, Mobile, Agent_Type, DueAmount, AgentLimit, amount, sRandomOTP, smsMsg, SmsCrd)
                    objSql.SmsLogDetails(Execid, Mobile, smsMsg, smsStatus)
                End If

            Catch ex As Exception
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Private Function GenerateRandomOTP(ByVal iOTPLength As Integer, ByVal saAllowedCharacters As String()) As String
        Dim sOTP As String = String.Empty
        Dim sTempChars As String = String.Empty
        Dim rand As Random = New Random()

        For i As Integer = 0 To iOTPLength - 1
            Dim p As Integer = rand.[Next](0, saAllowedCharacters.Length)
            sTempChars = saAllowedCharacters(rand.[Next](0, saAllowedCharacters.Length))
            sOTP += sTempChars
        Next

        Return sOTP
    End Function

    Private Function InsertOtp(ByVal userid As String, ByVal transid As String, ByVal sRandomOTP As String, ByVal MbNo As String, ByVal reason As String) As Integer
        Dim a As Integer = STDom.InsertOTP(userid, transid, sRandomOTP, MbNo, reason)
        Return a
    End Function

End Class
